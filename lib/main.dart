import 'package:vmscustomer/screens/chatroom_screen.dart';
import 'package:vmscustomer/screens/detaildriver_screen.dart';
import 'package:vmscustomer/screens/detailhistory_screen.dart';
import 'package:vmscustomer/screens/detailorder_screen.dart';
import 'package:vmscustomer/screens/history_screen.dart';
import 'package:vmscustomer/screens/home_screen.dart';
import 'package:vmscustomer/screens/login_screen.dart';
import 'package:vmscustomer/screens/order_screen.dart';
import 'package:vmscustomer/screens/register_screen.dart';
import 'package:vmscustomer/screens/splash_screen.dart';
import 'package:vmscustomer/screens/utama_screen.dart';
import 'package:vmscustomer/screens/waitingdriver_screen.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    SplashScreen.tag: (context) => SplashScreen(),
    LoginScreen.tag: (context) => LoginScreen(),
    MyHome.tag: (context) => MyHome(),
    UtamaScreen.tag: (context) => UtamaScreen(),
    RegisterScreen.tag: (context) => RegisterScreen(),
    OrderScreen.tag: (context) => OrderScreen(),
    //DetailOrderScreen.tag: (context) => DetailOrderScreen(),
    //WaitingDriverScreen.tag: (context) => WaitingDriverScreen()
    //DetailDriverScreen.tag: (context) => DetailDriverScreen()
    HistoryScreen.tag: (context) => HistoryScreen(),
    // DetailHistory.tag: (context) => DetailHistory()
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.red[700],
        scaffoldBackgroundColor: Colors.grey[200],
        appBarTheme: AppBarTheme(
          color: Colors.red[700],
        ),
      ),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        if (settings.name == WaitingDriverScreen.tag) {
          return MaterialPageRoute(builder: (context) {
            return WaitingDriverScreen(
              idOrder: settings.arguments,
            );
          });
        } else if (settings.name == DetailDriverScreen.tag) {
          return MaterialPageRoute(builder: (context) {
            return DetailDriverScreen(
              idOrder: settings.arguments,
            );
          });
        } else if (settings.name == DetailHistory.tag) {
          return MaterialPageRoute(builder: (context) {
            return DetailHistory(
              dataHistoryOrder: settings.arguments,
            );
          });
        } else if (settings.name == DetailOrderScreen.tag) {
          return MaterialPageRoute(builder: (context) {
            return DetailOrderScreen(
              dataDetailOrder: settings.arguments,
            );
          });
        } else if (settings.name == ChatRoom.tag) {
          return MaterialPageRoute(builder: (context) {
            return ChatRoom(
              dataChatOrder: settings.arguments,
            );
          });
        }
      },
      home: SplashScreen(),
      routes: routes,
    );
  }
}

// FLUTTER DAY 1
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: SplashPage(),
//     );
//   }
// }

// class SplashPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return new SplashScreen(
//         seconds: 3,
//         navigateAfterSeconds: new MyHome(),
//         title: new Text('Welcome to PT Hutama Karya'),
//         image:
//             new Image.network("https://hutamakarya.com/storage/logo-site.png"),
//         backgroundColor: Colors.white,
//         styleTextUnderTheLoader: new TextStyle(),
//         photoSize: 100.0,
//         loaderColor: Colors.red);
//   }
// }

// class MyHome extends StatefulWidget {
//   @override
//   _MyHomeState createState() => _MyHomeState();
// }

// class _MyHomeState extends State<MyHome> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("flutter app"),
//       ),
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Container(
//             margin: EdgeInsets.only(top: 20),
//             child: Center(
//                 child: Image.asset(
//               "images/logo-site.png",
//               width: 300,
//               height: 215,
//             )),
//           ),
//           Padding(
//             padding: const EdgeInsets.all(10),
//             child: Text(
//               "Hutama Karya merupakan Badan Usaha Milik Negara (BUMN) yang bergerak dibidang jasa konstruksi, pengembangan dan penyedia jasa jalan tol.",
//               style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
//             ),
//           ),
//           Container(
//               margin: EdgeInsets.only(top: 10),
//               child: Center(
//                 child: RaisedButton(
//                   color: Colors.red,
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(30)),
//                   onPressed: () {
//                     Toast.show("Sudah diklik", context,
//                         duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
//                   },
//                   child: Text(
//                     "Get Started",
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ))
//         ],
//       ),
//     );
//   }
// }

import 'package:vmscustomer/models/model_orderchat.dart';
import 'package:flutter/material.dart';
import 'package:vmscustomer/widgets/my_chat_item.dart';

class ChatOrder extends StatelessWidget {
  List<DataChatOrder> dataChatOrder;
  ChatOrder({Key key, this.dataChatOrder = null}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataChatOrder?.length ?? 0,
      itemBuilder: (context, index) => ChatItem(
        dataChatOrder: dataChatOrder[index],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:vmscustomer/models/model_getprofile.dart';

class MyAccount extends StatelessWidget {
  DataProfile dataProfile;
  MyAccount({Key key, this.dataProfile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: tampilanProfile(),
    );
  }

  Widget tampilanProfile() {
    return Column(
      children: [
        Container(
          height: 60,
          color: Colors.grey[200],
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Login and Security",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        itemProfile(
          Colors.white,
          "Email",
          dataProfile?.email ?? "",
          FontWeight.normal,
          null,
          Colors.black,
        ),
        // Container(
        //   height: 50,
        //   color: Colors.white,
        //   child: Padding(
        //     padding: const EdgeInsets.all(8.0),
        //     child: Row(
        //       children: [
        //         Container(
        //           child: Text(
        //             "Make sure to change your password regulary for privacy and safety",
        //             style: TextStyle(
        //               fontWeight: FontWeight.bold,
        //               fontSize: 11,
        //               color: Colors.black,
        //             ),
        //           ),
        //         ),
        //       ],
        //     ),
        //   ),
        // ),
        Container(
          height: 60,
          color: Colors.grey[200],
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Account",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        itemProfile(
          Colors.white,
          "Nama",
          dataProfile?.namaKaryawan ?? "",
          FontWeight.normal,
          null,
          Colors.black,
        ),
        itemProfile(
          Colors.white,
          "NIK",
          dataProfile?.nikKaryawan ?? "",
          FontWeight.normal,
          null,
          Colors.black,
        ),
        itemProfile(
          Colors.white,
          "Divisi",
          dataProfile?.namaDivisi ?? "",
          FontWeight.normal,
          null,
          Colors.black,
        ),
      ],
    );
  }

  //Widget Setter Title
  Widget itemProfile(
    Color bgcolor,
    String title,
    String item,
    FontWeight fontWeight,
    double fontSize,
    Color fontColor,
  ) {
    return Container(
      height: 50,
      color: bgcolor,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Container(
              width: 60,
              child: Text(
                title,
                style: TextStyle(
                  fontWeight: fontWeight,
                  fontSize: fontSize,
                  color: fontColor,
                ),
              ),
            ),
            Flexible(
              child: Text(
                item,
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 15,
                  color: Colors.grey,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

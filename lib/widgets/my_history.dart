import 'package:vmscustomer/models/model_orderhistory.dart';
import 'package:vmscustomer/widgets/my_history_item.dart';
import 'package:flutter/material.dart';

class History extends StatelessWidget {
  List<DataHistoryOrder> dataHistoryOrder;
  History({Key key, this.dataHistoryOrder = null}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataHistoryOrder?.length ?? 0,
      itemBuilder: (context, index) => HistoryItem(
        dataHistoryOrder: dataHistoryOrder[index],
      ),
    );
  }
}

import 'package:vmscustomer/models/model_ordernotification.dart';
import 'package:flutter/material.dart';
import 'package:vmscustomer/widgets/my_notif_item.dart';

class NotifOrder extends StatelessWidget {
  List<DataNotifOrder> dataNotifOrder;
  NotifOrder({Key key, this.dataNotifOrder = null}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataNotifOrder?.length ?? 0,
      itemBuilder: (context, index) => NotifItem(
        dataNotifOrder: dataNotifOrder[index],
      ),
    );
  }
}

import 'package:vmscustomer/models/model_ordernotification.dart';
import 'package:flutter/material.dart';

class NotifItem extends StatelessWidget {
  DataNotifOrder dataNotifOrder;
  NotifItem({Key key, this.dataNotifOrder}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 7,
      child: Column(
        children: [
          Container(
            color: Colors.white,
            height: 30,
            width: double.infinity,
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(dataNotifOrder?.orderCode ?? "Kosong",
                    textAlign: TextAlign.end,
                    style: TextStyle(color: Colors.black)),
              ),
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 4),
            child: Row(
              children: [
                Icon(
                  Icons.notifications,
                  size: 30,
                  color: Colors.blue[900],
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      dataNotifOrder.notif,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Container(
            width: double.infinity,
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Text(
                  dataNotifOrder?.tglNotif ?? "Kosong",
                  textAlign: TextAlign.end,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

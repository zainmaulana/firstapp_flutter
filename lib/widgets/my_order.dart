import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_ordergetdetail.dart';
import 'package:vmscustomer/models/model_orderhistory.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/detaildriver_screen.dart';
import 'package:vmscustomer/screens/detailorder_screen.dart';
import 'package:vmscustomer/screens/history_screen.dart';
import 'package:vmscustomer/screens/order_screen.dart';
import 'package:vmscustomer/screens/detailhistory_screen.dart';

class MyOrder extends StatefulWidget {
  DataDetailOrder dataDetailOrder;
  String idOrder;
  MyOrder({Key key, this.dataDetailOrder, this.idOrder}) : super(key: key);
  @override
  _MyOrderState createState() => _MyOrderState();
}

class _MyOrderState extends State<MyOrder> {
  DataHistoryOrder dataHistoryOrder;
  int iduser, idorder;
  String token, device;
  String tipeOrder, tujuan;
  dynamic argument;
  NetworkOjol networkOjol = NetworkOjol();
  DateTime _selectedDate;
  TimeOfDay _selectedTime;

  // Timer _timer;
  // int _start = 10;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
    // startTimer();

    _selectedDate = DateTime.now();
    _selectedTime = TimeOfDay.now();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    idorder = pref.getInt("idorder");
    device = await getID();
    getHistoryEach(idorder.toString());
    print("ID Order User : " + idorder.toString());
  }

  // void startTimer() {
  //   const oneSec = const Duration(seconds: 3);
  //   _timer = new Timer.periodic(
  //     oneSec,
  //     (Timer timer) => setState(
  //       () {
  //         if (_start > 1) {
  //           setStateOrder();
  //           getHistoryEach(idorder.toString());

  //           print("Refresh State");
  //         }
  //       },
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: menu(),
    );
  }

  //Widget Menu
  Widget menu() {
    return Column(
      children: [
        Container(
          height: 50,
          width: double.infinity,
          child: Align(
            alignment: Alignment.center,
            child: Text(
              "Where do you want to go ?",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.black,
              ),
            ),
          ),
        ),
        Container(
          height: 30,
          width: double.infinity,
          padding: const EdgeInsets.only(left: 20),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "My Order",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: Colors.grey,
              ),
            ),
          ),
        ),
        setStateOrder(),
        Container(
          height: 30,
          width: double.infinity,
          padding: const EdgeInsets.only(left: 20),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Important Notice",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: Colors.grey,
              ),
            ),
          ),
        ),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 20, 15),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(5),
                image: DecorationImage(
                  image: AssetImage(
                    "images/bg_notice.PNG",
                  ),
                  fit: BoxFit.fill,
                ),
              ),
              width: double.infinity,
              child: Container(),
            ),
          ),
        ),
        Container(
          height: 30,
          width: double.infinity,
          padding: const EdgeInsets.only(left: 20),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Order List",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: Colors.grey,
              ),
            ),
          ),
        ),
        Flexible(
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(HistoryScreen.tag);
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 5, 20, 15),
              child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  height: 50,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Icon(
                          Icons.history,
                        ),
                      ),
                      Text(
                        "View your order history",
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      ],
    );
  }

  getHistoryEach(String idOrderEach) {
    networkOjol
        .getHistoryOrderEach(
      iduser.toString(),
      idOrderEach,
    )
        .then((response) {
      if (response.result == "true") {
        print("hasil " + response.data[0]?.idOrder.toString() ?? "");
        setState(() {
          dataHistoryOrder = response?.data[0] ?? "";
        });
      } else {
        print("tidak ada hasil");
        setState(() {
          dataHistoryOrder = null;
        });
      }
    });
  }

  Widget setStateOrder() {
    if (widget.idOrder == "true") {
      if (widget.dataDetailOrder.idOrderType == 1 &&
          widget.dataDetailOrder.idOrderStatus < 4) {
        tipeOrder = "Now Order";
        tujuan = DetailDriverScreen.tag;
        argument = widget.dataDetailOrder.idOrder.toString();
      } else if (widget.dataDetailOrder.idOrderType == 2 &&
          widget.dataDetailOrder.idOrderStatus < 3) {
        tipeOrder = "On Schedule";
        tujuan = DetailOrderScreen.tag;
        argument = widget.dataDetailOrder;
      } else if (widget.dataDetailOrder.idOrderType == 2 &&
          widget.dataDetailOrder.idOrderStatus == 3) {
        tipeOrder = "On Schedule";
        tujuan = DetailDriverScreen.tag;
        argument = widget.dataDetailOrder.idOrder.toString();
      } else if (widget.dataDetailOrder.idOrderType == 1 &&
          widget.dataDetailOrder.idOrderStatus == 4) {
        getHistoryEach(widget.dataDetailOrder.idOrder.toString());
        tipeOrder = "Now Order";
        tujuan = DetailHistory.tag;
        argument = dataHistoryOrder;
      } else if (widget.dataDetailOrder.idOrderType == 2 &&
          widget.dataDetailOrder.idOrderStatus == 4) {
        getHistoryEach(widget.dataDetailOrder.idOrder.toString());
        tipeOrder = "On Schedule";
        tujuan = DetailHistory.tag;
        argument = dataHistoryOrder;
      } else {
        return makeOrder();
      }
      return tampilanOrder(
        tipeOrder,
        tujuan,
        argument,
      );
    } else {
      if (_selectedTime.hour >= 7 && _selectedTime.hour <= 16) {
        return makeOrder();
      } else {
        return closeOrder();
      }
    }
  }

  //Widget Setter Menu
  Widget makeOrder() {
    return Flexible(
        child: GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(OrderScreen.tag);
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 15),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            height: double.infinity,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Icon(
                    Icons.car_repair,
                  ),
                ),
                Text(
                  "Make a New Order",
                  style: TextStyle(fontSize: 20),
                ),
              ],
            )),
      ),
    ));
  }

  Widget tampilanOrder(
    String tipeOrder,
    String tujuan,
    dynamic argument,
  ) {
    return Flexible(
      child: GestureDetector(
        onTap: () {
          //Toast.show(widget.dataDetailOrder.orderCode, context);
          Navigator.of(context).pushNamed(
            tujuan,
            arguments: argument,
          );
        },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 5, 20, 15),
          child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              height: double.infinity,
              width: double.infinity,
              child: Row(
                children: [
                  Icon(
                    Icons.car_repair,
                    size: 60,
                    color: Colors.red[700],
                  ),
                  Expanded(
                      child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 2),
                              child: Icon(
                                Icons.trip_origin,
                                size: 30,
                                color: Colors.red[700],
                              ),
                            ),
                            Flexible(
                              child: Text(
                                widget.dataDetailOrder?.pickupLocation ??
                                    "Kosong",
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 2),
                              child: Icon(
                                Icons.place,
                                size: 30,
                                color: Colors.green,
                              ),
                            ),
                            Flexible(
                              child: Text(
                                widget.dataDetailOrder?.dropLocation ??
                                    "Kosong",
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 30),
                            child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  side: BorderSide(color: Colors.red)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  tipeOrder,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.red[700],
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Text(
                              widget.dataDetailOrder?.startDate ?? "Kosong",
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Flexible(
                        child: ratingCheck(),
                      ),
                    ],
                  )),
                ],
              )),
        ),
      ),
    );
  }

  Widget closeOrder() {
    return Flexible(
        child: Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 15),
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          height: double.infinity,
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Icon(
                  Icons.car_repair,
                ),
              ),
              Flexible(
                child: Text(
                  "Closed Service. (Open on 07.00 - 17.00)",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          )),
    ));
  }

  Widget ratingCheck() {
    if (widget.dataDetailOrder.idRating == null &&
        widget.dataDetailOrder.idOrderStatus == 4) {
      return Padding(
        padding: const EdgeInsets.only(right: 5),
        child: Row(
          children: [
            Expanded(
              child: Card(
                color: Colors.red[700],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: Colors.red[700])),
                child: Center(
                  child: Text(
                    "Nilai Sekarang : " + widget.dataDetailOrder?.orderCode ??
                        "",
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(right: 5),
        child: Row(
          children: [
            Expanded(
              child: Card(
                color: Colors.green,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: Colors.green)),
                child: Center(
                  child: Text(
                    "Ke Detail Order : " + widget.dataDetailOrder?.orderCode ??
                        "",
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }
}

import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmscustomer/models/model_orderchat.dart';
import 'package:flutter/material.dart';
import 'package:vmscustomer/screens/chatroom_screen.dart';

class ChatItem extends StatelessWidget {
  DataChatOrder dataChatOrder;
  ChatItem({Key key, this.dataChatOrder}) : super(key: key);

  Future<void> setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setInt("idorder", dataChatOrder.idOrder);
    print("ID Order : " + dataChatOrder.idOrder.toString());
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setPref();
        //Toast.show(dataChatOrder.orderCode, context);
        Navigator.of(context).pushNamed(ChatRoom.tag, arguments: dataChatOrder);
      },
      child: Card(
        elevation: 7,
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: 30,
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(dataChatOrder.orderCode,
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.black)),
                ),
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                        child: Row(
                          children: [
                            Icon(
                              Icons.person,
                              size: 30,
                              color: Colors.green,
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  dataChatOrder.namaDriver,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                        child: Row(
                          children: [
                            Icon(
                              Icons.drive_eta,
                              size: 30,
                              color: Colors.black,
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  dataChatOrder.nopolUnit,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                        child: Row(
                          children: [
                            Icon(
                              Icons.airport_shuttle,
                              size: 30,
                              color: Colors.red[700],
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  dataChatOrder.namaUnit,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 8, right: 4, bottom: 4),
                    child: Row(
                      children: [
                        Icon(
                          Icons.chat_outlined,
                          size: 30,
                          color: Colors.red[700],
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(
                              dataChatOrder.lastChat,
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    dataChatOrder.orderDate,
                    textAlign: TextAlign.end,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

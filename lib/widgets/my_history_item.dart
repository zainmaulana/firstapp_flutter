import 'package:vmscustomer/models/model_orderhistory.dart';
import 'package:vmscustomer/screens/detailhistory_screen.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class HistoryItem extends StatelessWidget {
  DataHistoryOrder dataHistoryOrder;
  HistoryItem({Key key, this.dataHistoryOrder}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        //Toast.show(dataHistoryOrder.orderCode, context);
        Navigator.of(context)
            .pushNamed(DetailHistory.tag, arguments: dataHistoryOrder);
      },
      child: Card(
        elevation: 7,
        child: Column(
          children: [
            Container(
              color: Colors.red[700],
              height: 30,
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(dataHistoryOrder?.orderCode ?? "Kosong",
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.white)),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            ratingIndicator(),
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 4),
              child: Row(
                children: [
                  Icon(
                    Icons.place,
                    size: 30,
                    color: Colors.green,
                  ),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Text(
                        dataHistoryOrder.dropLocation,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    "Tanggal Order : " + dataHistoryOrder?.orderDate ??
                        "Kosong",
                    textAlign: TextAlign.end,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget ratingIndicator() {
    if (dataHistoryOrder.idRating == null &&
        dataHistoryOrder.idOrderStatus != 5) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Belum Dinilai",
            style: TextStyle(
              color: Colors.red[700],
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}

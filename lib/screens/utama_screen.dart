import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:toast/toast.dart';
import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_ordergetdetail.dart';
import 'package:vmscustomer/models/model_getprofile.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/inbox_screen.dart';
import 'package:vmscustomer/screens/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:vmscustomer/widgets/my_account.dart';
import 'package:vmscustomer/widgets/my_order.dart';

class UtamaScreen extends StatefulWidget {
  static String tag = 'utama-page';

  @override
  _UtamaScreenState createState() => _UtamaScreenState();
}

class _UtamaScreenState extends State<UtamaScreen>
    with SingleTickerProviderStateMixin {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _homeScreenText = "Waiting for token...";
  String fcm;
  String tokenUpdate;
  int iduser;
  String token, device;
  NetworkOjol networkOjol = NetworkOjol();
  DataProfile dataProfile;
  DataDetailOrder dataDetailOrder;
  TabController controller;
  String idOrder;

  // Timer _timer;
  // int _start = 10;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = TabController(length: 3, vsync: this);
    controller.addListener(() {
      setState(() {
        switch (controller.index) {
          case 0:
            return getDetailOrder();
            break;
          case 1:
            return;
            break;
          case 2:
            return getProfile();
            break;
        }
      });
    });
    getPref();
    // startTimer();

    //Firebase Configuration
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        // _showItemDialog(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // _navigateToItemDetail(message);
        // _showItemDialog(message);

        // if (click == true) {
        //   _showItemDialog(message);
        // }
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // _navigateToItemDetail(message);
        // _showItemDialog(message);
        // _showItemDialog(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: true),
    );
    _firebaseMessaging.onIosSettingsRegistered.listen(
      (IosNotificationSettings settings) {
        print("Settings registered: $settings");
      },
    );
    //Function untuk Get Token FCM dari Firebase
    _firebaseMessaging.getToken().then(
      (String token) {
        assert(token != null);
        setState(() {
          tokenUpdate = token;
          _homeScreenText = "Push Messaging Token : $token";
        });
        print(_homeScreenText);
        setTokentoPref(tokenUpdate);
        insertTokentoDb();
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getID();
    print("ID User : " + iduser.toString());
    getProfile();
    getDetailOrder();
  }

  Future<void> setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setInt("idorder", dataDetailOrder.idOrder);
    print("ID Order : " + dataDetailOrder.idOrder.toString());
  }

  // void startTimer() {
  //   const oneSec = const Duration(seconds: 1);
  //   _timer = new Timer.periodic(
  //     oneSec,
  //     (Timer timer) => setState(
  //       () {
  //         if (_start > 1) {
  //           getProfile();
  //           getDetailOrder();

  //           print("Refresh State");
  //         }
  //       },
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("My Home"),
        actions: [
          IconButton(
              icon: Icon(Icons.logout),
              onPressed: () async {
                SharedPreferences pref = await SharedPreferences.getInstance();
                pref.clear();
                Navigator.popAndPushNamed(context, LoginScreen.tag);
              })
        ],
      ),
      body: Center(
        child: TabBarView(controller: controller, children: <Widget>[
          RefreshIndicator(
              child: Container(
                child: MyOrder(
                  dataDetailOrder: dataDetailOrder,
                  idOrder: idOrder,
                ),
              ),
              onRefresh: () => getDetailOrder()),
          InboxTabBar(),
          RefreshIndicator(
              child: Container(
                child: MyAccount(
                  dataProfile: dataProfile,
                ),
              ),
              onRefresh: () => getProfile()),
        ]),
      ),
      bottomNavigationBar: Material(
        color: Colors.white,
        child: TabBar(
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.post_add),
              text: "My Order",
            ),
            Tab(
              icon: Icon(Icons.message),
              text: "My Inbox",
            ),
            Tab(
              icon: Icon(Icons.account_box),
              text: "My Account",
            ),
          ],
          controller: controller,
          indicatorColor: Colors.red[700],
          labelColor: Colors.red[700],
        ),
      ),
    );
  }

  getProfile() {
    networkOjol.getProfileUser(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("Email User : " + response.data.email);
        //Toast.show(response.message, context);
        setState(() {
          dataProfile = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        //Toast.show(response.message, context);
        setState(() {
          dataProfile = null;
        });
      }
    });
  }

  getDetailOrder() {
    networkOjol.getDetailOrderUser(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("Order : " + response.data.orderCode);
        //Toast.show(response.message, context);
        setState(() async {
          dataDetailOrder = response.data;
          idOrder = "true";
          // SharedPreferences pref = await SharedPreferences.getInstance();
          // pref.setString("idorder", response.data.idOrder.toString());
        });
      } else {
        print("Tidak Ada Hasil");
        //Toast.show(response.message, context);
        setState(() {
          dataDetailOrder = null;
          idOrder = "false";
        });
      }
    });
  }

  //Fungsi untuk set Token FCM jika sebelumnya kosong
  setTokentoPref(String tokenUpdate) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("FCM : ", tokenUpdate);
    print("Token Anda :" + tokenUpdate);
  }

  //Fungsi untuk Insert Token FCM ke DB
  void insertTokentoDb() {
    networkOjol
        .registerFcm(
      iduser.toString(),
      tokenUpdate,
    )
        .then((response) {
      if (response.result == "true") {
        print("Berhasil Insert FCM : " + tokenUpdate);
        //Toast.show(response.message, context);
      } else {
        print("Gagal Insert FCM");
        //Toast.show(response.message, context);
      }
    });
  }
}

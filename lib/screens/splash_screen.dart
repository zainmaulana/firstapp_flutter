import 'dart:async';
import 'package:vmscustomer/screens/login_screen.dart';
import 'package:vmscustomer/screens/utama_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static String tag = 'splash-screen';
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    startSplashScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "images/vms-icon2.png",
              width: 300,
              height: 300,
            ),
            Text(
              "Vehicle Management System",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Colors.red[700],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "for User",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.red[700],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CircularProgressIndicator(
              backgroundColor: Colors.white,
            )
          ],
        ),
      ),
    );
  }

  startSplashScreen() {
    var duration = Duration(seconds: 3);
    return Timer(duration, () async {
      //aksi ketika durasi selesai -> pindah halaman
      SharedPreferences pref = await SharedPreferences.getInstance();
      bool session = (pref.getBool('session') ?? false);
      if (session) {
        Navigator.of(context).pushReplacementNamed(UtamaScreen.tag);
      } else {
        Navigator.of(context).pushReplacementNamed(LoginScreen.tag);
      }
    });
  }
}

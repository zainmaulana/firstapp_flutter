import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_orderchat.dart';
import 'package:vmscustomer/models/model_ordernotification.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/widgets/my_chat.dart';
import 'package:vmscustomer/widgets/my_notif.dart';

class InboxTabBar extends StatefulWidget {
  @override
  _InboxTabBarState createState() => _InboxTabBarState();
}

class _InboxTabBarState extends State<InboxTabBar>
    with TickerProviderStateMixin {
  NetworkOjol networkOjol = NetworkOjol();
  int iduser;
  String token, device;
  List<DataNotifOrder> dataNotifOrder;
  List<DataChatOrder> dataChatOrder;
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: 2, vsync: this);
    controller.addListener(() {
      setState(() {
        switch (controller.index) {
          case 0:
            return getNotifications();
            break;
          case 1:
            return getEachChat();
            break;
        }
      });
    });
    getPref();
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   controller.dispose();
  // }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getID();
    print("ID User : " + iduser.toString());
    getNotifications();
    getEachChat();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(48),
        child: Material(
          color: Colors.white,
          child: TabBar(
            controller: controller,
            tabs: <Widget>[
              Tab(
                text: "NOTIFICATIONS",
              ),
              Tab(
                text: "MESSAGES",
              ),
            ],
            indicatorColor: Colors.red[700],
            labelColor: Colors.red[700],
          ),
        ),
      ),
      body: TabBarView(controller: controller, children: <Widget>[
        RefreshIndicator(
            child: Container(
              child: NotifOrder(
                dataNotifOrder: dataNotifOrder,
              ),
            ),
            onRefresh: () => getNotifications()),
        RefreshIndicator(
            child: Container(
              child: ChatOrder(
                dataChatOrder: dataChatOrder,
              ),
            ),
            onRefresh: () => getEachChat()),
      ]),
    );
  }

  getNotifications() {
    networkOjol.getNotifUser(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("ID User : " + response.data[0].idUser.toString());
        //Toast.show(response.message, context);
        setState(() {
          dataNotifOrder = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        //Toast.show(response.message, context);
        setState(() {
          dataNotifOrder = null;
        });
      }
    });
  }

  getEachChat() {
    networkOjol.getEachChat(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("ID User : " + response.data[0].idUser.toString());
        //Toast.show(response.message, context);
        setState(() {
          dataChatOrder = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        //Toast.show(response.message, context);
        setState(() {
          dataChatOrder = null;
        });
      }
    });
  }
}

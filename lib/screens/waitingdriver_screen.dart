import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_ordergetdetail.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/detaildriver_screen.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_cache_builder.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:vmscustomer/screens/utama_screen.dart';

class WaitingDriverScreen extends StatefulWidget {
  String idOrder;

  WaitingDriverScreen({Key key, this.idOrder = null}) : super(key: key);
  static String tag = 'waitingdriver-page';
  @override
  _WaitingDriverScreenState createState() => _WaitingDriverScreenState();
}

class _WaitingDriverScreenState extends State<WaitingDriverScreen> {
  Timer _timer;
  int _start = 10;
  final TextEditingController _cancelComment = TextEditingController();
  String komentarOrder;
  int iduser;
  String token, device;
  NetworkOjol networkOjol = NetworkOjol();
  final flareasset = AssetFlare(bundle: rootBundle, name: "images/Filip.flr");
  DataDetailOrder dataDetailOrder;
  int limitOrderDate;
  TimeOfDay _selectedTime;

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getID();
    print("ID User : " + iduser.toString());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
          appBar: AppBar(
            title: Text("Halaman Menunggu Driver"),
          ),
          body: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: FlareCacheBuilder([flareasset], builder: (context, b) {
                return b
                    ? FlareActor.asset(
                        flareasset,
                        alignment: Alignment.center,
                        animation: "idle",
                        antialias: true,
                      )
                    : Container();
              }))
            ],
          ))),
    );
  }

  Future<bool> _willPopCallback() async {
    // await showDialog or Show add banners or whatever
    // then
    tampilPilihan(context);
    return Future.value(true);
  }

  tampilPilihan(BuildContext context) {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("Konfirmasi Batal Order"),
          content: TextField(
            style: TextStyle(
              color: Colors.black,
            ),
            controller: _cancelComment,
            keyboardType: TextInputType.multiline,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                color: Colors.red[700],
              )),
            ),
          ),
          actions: [
            new TextButton(
              child: new Text(
                "Tutup",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new TextButton(
              child: new Text(
                "Cancel",
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                setState(() {
                  komentarOrder = _cancelComment.text;
                });
                batalOrder();
              },
            ),
          ],
        ));
  }

  //Popup Gage
  tampilNonGage(BuildContext context) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        child: new AlertDialog(
          title: new Text("Konfirmasi Pakai Kendaraan Non Ganjil Genap"),
          content: Text(
              "Mohon Maaf Menunggu karena Kendaraan Ganjil Genap Tidak Tersedia. Apakah Anda bersedia untuk menggunakan Mobil Lainnya ?"),
          actions: [
            new TextButton(
              child: new Text(
                "Tidak",
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                batalOrderGage();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    UtamaScreen.tag, (Route<dynamic> route) => false);
              },
            ),
            new TextButton(
              child: new Text(
                "Bersedia",
                style: TextStyle(
                  color: Colors.green,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                insertOrder();
                batalOrderGage();
              },
            ),
          ],
        ));
  }

  //Popup Gage NonAktif
  tampilTidakTersedia(BuildContext context) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        child: new AlertDialog(
          title: new Text("Konfirmasi Ketersediaan Kendaraan"),
          content: Text(
              "Mohon Maaf Menunggu karena Kendaraan Tidak Tersedia. Mohon untuk Mengontak Admin Carpooling"),
          actions: [
            new TextButton(
              child: new Text(
                "Kembali ke Menu Utama",
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                batalOrderKosong();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    UtamaScreen.tag, (Route<dynamic> route) => false);
              },
            ),
          ],
        ));
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 3);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start > 1) {
            _selectedTime = TimeOfDay.now();

            getPref();
            checkOrder();
            setDetailOrder();

            print("Refresh State");
          }
        },
      ),
    );
  }

  void setDetailOrder() {
    getCheckDetailOrder();

    //Matching Time
    limitOrderDate =
        int.parse(dataDetailOrder.limitOrderDate.substring(14, 16));
    print("ID Order : " + dataDetailOrder.idOrder.toString());
    print("Menit Limit Order Date : " + limitOrderDate.toString());
    print("Menit Sekarang : " + _selectedTime.minute.toString());

    if (limitOrderDate < _selectedTime.minute) {
      setState(() {
        tampilTidakTersedia(context);
        // tampilNonGage(context);
        _timer.cancel();
      });
    } else {}
  }

  void checkOrder() {
    networkOjol.checkOrder(widget.idOrder).then((response) {
      print("ID Order : " + widget.idOrder);
      if (response.result == "true") {
        print(response.result.toString() + response.message.toString());
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
          DetailDriverScreen.tag,
          (Route<dynamic> route) => false,
          arguments: response.data.idOrder.toString(),
        );
        // Navigator.pushReplacementNamed(context, DetailDriverScreen.tag,
        //     arguments: response.data.idOrder.toString());
        _timer.cancel();
        print("Berhasil Menemukan Driver");
      } else {
        Toast.show(
            "Mohon Ditunggu, Driver akan Segera Menerima Order Anda", context);
        print("Belum Berhasil Menemukan Driver");
      }
    });
  }

  void getCheckDetailOrder() {
    networkOjol.getCheckDetailOrderUser(iduser.toString()).then((response) {
      if (response.result == "true") {
        print("Order : " + response.data.orderCode);
        //Toast.show(response.message, context);
        setState(() async {
          dataDetailOrder = response.data;
        });
      } else {
        print("Tidak Ada Hasil");
        //Toast.show(response.message, context);
        setState(() {
          dataDetailOrder = null;
        });
      }
    });
  }

  void insertOrder() {
    networkOjol
        .insertOrder(
      dataDetailOrder.idOrderStatus.toString(),
      dataDetailOrder.idOrderType.toString(),
      iduser.toString(),
      dataDetailOrder.jumlahPenumpang.toString(),
      dataDetailOrder.keperluan,
      dataDetailOrder.pickupLatitude,
      dataDetailOrder.pickupLongitude,
      dataDetailOrder.pickupLocation,
      dataDetailOrder.dropLatitude,
      dataDetailOrder.dropLongitude,
      dataDetailOrder.dropLocation,
      dataDetailOrder.realDistance,
      dataDetailOrder.realDuration,
      dataDetailOrder.catatan,
      dataDetailOrder.startDate,
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        if (dataDetailOrder.idOrderType == 1) {
          Navigator.of(context).pushNamedAndRemoveUntil(
            WaitingDriverScreen.tag,
            (Route<dynamic> route) => false,
            arguments: response.data.idOrder.toString(),
          );
        } else if (dataDetailOrder.idOrderType == 2) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              UtamaScreen.tag, (Route<dynamic> route) => false);
        }
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void batalOrder() {
    print("ID Order : " + widget.idOrder);
    print(komentarOrder);
    networkOjol
        .cancelOrder(
      widget.idOrder,
      komentarOrder,
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        _timer.cancel();
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void batalOrderGage() {
    print("ID Order : " + widget.idOrder);
    networkOjol
        .cancelOrderGage(
      widget.idOrder,
      "Dibatalkan karena Tidak Ganjil Genap",
    )
        .then((response) {
      if (response.result == "true") {
        print("Batal Order Kendaraan Gage");
        _timer.cancel();
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void batalOrderKosong() {
    print("ID Order : " + widget.idOrder);
    networkOjol
        .cancelOrder(
      widget.idOrder,
      "Dibatalkan karena Kendaraan Tidak Tersedia",
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        _timer.cancel();
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }
}

import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/utama_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

class LoginScreen extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();

  final FocusNode passwordNode = FocusNode();
  bool _obscoreText = true;
  NetworkOjol networkOjol = NetworkOjol();
  final _key = GlobalKey<ScaffoldState>();

  void _toggle() {
    setState(() {
      _obscoreText = !_obscoreText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        key: _key,
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: Column(
          children: [
            Hero(
              tag: "hero",
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 96.0,
                child: Image.asset("images/vms-icon2.png"),
              ),
            ),
            myTextField(
                _email,
                null,
                TextInputType.emailAddress,
                "Email Korporat",
                "Email",
                Icon(Icons.person),
                null,
                false,
                passwordNode),
            myTextField(
                _password,
                passwordNode,
                TextInputType.visiblePassword,
                "Password Anda",
                "Password",
                Icon(Icons.lock),
                IconButton(
                    icon: Icon(
                        _obscoreText ? Icons.visibility : Icons.visibility_off),
                    onPressed: _toggle),
                _obscoreText,
                null),
            SizedBox(
              height: 30,
            ),
            MaterialButton(
              minWidth: 200,
              height: 42,
              onPressed: () {
                setState(() {
                  if (_email.text.isEmpty || _password.text.isEmpty) {
                    Toast.show("Tidak Boleh Kosong", context);
                  } else {
                    prosesLogin();
                  }
                });
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.red[700],
              child: Text(
                "Login",
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            TextButton(
              onPressed: () {
                url_launcher
                    .launch('https://vms.hutamakarya.com/password/reset');
              },
              child: Text(
                "Forgot/Reset Password ?",
                style: TextStyle(color: Colors.blue[900]),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> prosesLogin() async {
    String device = await getID();
    networkOjol
        .loginCustomer(_email.text, _password.text, device)
        .then((response) async {
      if (response.result == "true") {
        Toast.show(response.message, context);
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.setBool("session", true);
        pref.setString("token", response.token);
        pref.setInt("iduser", response.id);
        Navigator.of(context).popAndPushNamed(UtamaScreen.tag);
        print("ID User : " + response.id.toString());
      } else {
        var snackBar = SnackBar(content: Text(response.message));
        _key.currentState.showSnackBar(snackBar);
      }
    });
  }

  Widget myTextField(
      TextEditingController controller,
      FocusNode fromNode,
      TextInputType type,
      String hint,
      String label,
      Icon prefix,
      IconButton suffix,
      bool obs,
      FocusNode toNode) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
      child: TextField(
        style: TextStyle(
          color: Colors.black,
        ),
        controller: controller,
        focusNode: fromNode,
        keyboardType: type,
        obscureText: obs,
        onSubmitted: (value) {
          FocusScope.of(context).requestFocus(toNode);
        },
        decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
              color: Colors.white,
            )),
            fillColor: Colors.white,
            filled: true,
            prefixIcon: prefix,
            suffixIcon: suffix,
            hintText: hint,
            hintStyle: TextStyle(color: Colors.black),
            labelText: label,
            labelStyle: TextStyle(color: Colors.black)),
      ),
    );
  }
}

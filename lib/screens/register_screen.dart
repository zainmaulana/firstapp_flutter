import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';

class RegisterScreen extends StatefulWidget {
  static String tag = 'register-page';
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final FocusNode passwordNode = FocusNode();
  final FocusNode nameNode = FocusNode();
  bool _obscoreText = true;
  NetworkOjol networkOjol = NetworkOjol();
  final _key = GlobalKey<ScaffoldState>();

  void _toggle() {
    setState(() {
      _obscoreText = !_obscoreText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        key: _key,
        appBar: AppBar(
          title: Text("Register"),
        ),
        body: Column(
          children: [
            Hero(
              tag: "hero",
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 48.0,
                child: Image.asset("images/logo-site.png"),
              ),
            ),
            myTextField(
                _email,
                null,
                TextInputType.emailAddress,
                "Email Korporat",
                "Email",
                Icon(Icons.person),
                null,
                false,
                passwordNode),
            myTextField(
                _password,
                passwordNode,
                TextInputType.visiblePassword,
                "Password Anda",
                "Password",
                Icon(Icons.lock),
                IconButton(
                    icon: Icon(
                        _obscoreText ? Icons.visibility : Icons.visibility_off),
                    onPressed: _toggle),
                _obscoreText,
                nameNode),
            myTextField(_name, nameNode, TextInputType.text, "Nama Lengkap",
                "Name", Icon(Icons.person), null, false, null),
            SizedBox(
              height: 30,
            ),
            RaisedButton(
              onPressed: () {
                setState(() {
                  if (_email.text.isEmpty ||
                      _password.text.isEmpty ||
                      _name.text.isEmpty) {
                    Toast.show("Tidak Boleh Kosong", context);
                  } else {
                    // prosesRegister();
                  }
                });
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.blue[400],
              child: Text(
                "Register",
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  // void prosesRegister() {
  //   networkOjol
  //       .registerCustomer(_name.text, _email.text, _password.text)
  //       .then((response) {
  //     if (response.result == "true") {
  //       Navigator.of(context).pushReplacementNamed(LoginScreen.tag);
  //       Toast.show(response.msg, context);
  //     } else {
  //       Toast.show(response.msg, context);
  //     }
  //   });
  // }

  Widget myTextField(
      TextEditingController controller,
      FocusNode fromNode,
      TextInputType type,
      String hint,
      String label,
      Icon prefix,
      IconButton suffix,
      bool obs,
      FocusNode toNode) {
    return TextField(
      style: TextStyle(
        color: Colors.black,
      ),
      controller: controller,
      focusNode: fromNode,
      keyboardType: type,
      obscureText: obs,
      onSubmitted: (value) {
        FocusScope.of(context).requestFocus(toNode);
      },
      decoration: InputDecoration(
          focusedBorder:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
          prefixIcon: prefix,
          suffixIcon: suffix,
          hintText: hint,
          hintStyle: TextStyle(color: Colors.black),
          labelText: label,
          labelStyle: TextStyle(color: Colors.black)),
    );
  }
}

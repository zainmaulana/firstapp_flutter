import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_ordergetkeperluan.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/detaildriver_screen.dart';
import 'package:vmscustomer/screens/utama_screen.dart';
import 'package:vmscustomer/screens/waitingdriver_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:indonesia/indonesia.dart';
import 'package:location/location.dart';
import 'package:search_map_place/search_map_place.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class OrderScreen extends StatefulWidget {
  static String tag = 'order-page';
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen>
    with SingleTickerProviderStateMixin {
  //Icon Marker
  BitmapDescriptor oriLocIcon;
  BitmapDescriptor destLocIcon;
  BitmapDescriptor pinIcon;
  String apiKey =
      "AIzaSyB87Z7EaGa873QQ6RrF5QaAA5E42pxu-jI"; //AIzaSyA961qUUYeU2tnBuk4gS1fpiXVjnCFnbcQ
  Set<Marker> _markers = Set<Marker>();
  Location loc = Location();
  LocationData currLoc;
  LocationData oriLatLang;
  LocationData destLatLang;
  double CAMERA_ZOOM = 16;
  double CAMERA_TILT = 80;
  double CAMERA_BEARING = 30;
  Completer<GoogleMapController> _controller = Completer();
  bool isShowPinMarker = false;
  bool isSelectOrigin = false;
  bool isSelectDest = false;
  bool isReviewRouteBeforeOrder = false;
  bool isReadyToCreateNewOrder = true;
  String oriAddress;
  String destAddress;
  TextEditingController _catatan = TextEditingController();
  String catatanFinal;
  String keperluan = 'Dinas Dalam Kota';
  int jumlahPenumpang = 1;
  int orderType = 0;
  int orderStatus = 1;

  List<DataKeperluan> dataKeperluan;

  TabController controller;
  final TextEditingController _selectingDate = TextEditingController();
  final TextEditingController _selectingTime = TextEditingController();
  String _newSchedule;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  DateTime _selectedDateNow;
  TimeOfDay _selectedTimeNow;
  String hariNow;
  String jamNow;
  String jamKurang10;
  String menitKurang10;
  String jamKurang10Now;
  String menitKurang10Now;

  double duration = 0;
  double distance = 0;
  int cost = 0;
  PolylinePoints polylinePoints = PolylinePoints();
  List<LatLng> polylineCoordinates = [];
  Set<Polyline> polylines = Set<Polyline>();

  NetworkOjol networkOjol = NetworkOjol();
  int iduser;
  String token, device;

  @override
  void initState() {
    super.initState();
    setMarkerIcon();
    setInitialLocation();
    getPref();
    getKeperluan();

    controller = TabController(length: 1, vsync: this);
    controller.addListener(() {
      setState(() {
        switch (controller.index) {
          case 0:
            return orderType = 1;
            break;
        }
      });
    });

    _selectedDate = DateTime.now();
    _selectedTime = TimeOfDay.now();

    _selectedDateNow = DateTime.now();
    _selectedTimeNow = TimeOfDay.now();

    //Order Now
    hariNow = _selectedDateNow.toLocal().year.toString() +
        "-" +
        _selectedDateNow.toLocal().month.toString() +
        "-" +
        _selectedDateNow.toLocal().day.toString();

    if (_selectedTimeNow.hour < 10) {
      jamKurang10Now = "0" + _selectedTimeNow.hour.toString();
      if (_selectedTimeNow.minute < 10) {
        menitKurang10Now = "0" + _selectedTimeNow.minute.toString();
        jamNow = jamKurang10Now + ":" + menitKurang10Now + ":00";
      } else {
        jamNow =
            jamKurang10Now + ":" + _selectedTimeNow.minute.toString() + ":00";
      }
    } else if (_selectedTimeNow.minute < 10) {
      menitKurang10Now = "0" + _selectedTimeNow.minute.toString();
      jamNow =
          _selectedTimeNow.hour.toString() + ":" + menitKurang10Now + ":00";
    } else {
      jamNow = _selectedTimeNow.hour.toString() +
          ":" +
          _selectedTimeNow.minute.toString() +
          ":00";
    }

    //On Schedule
    _selectingDate.text = _selectedDate.toLocal().year.toString() +
        "-" +
        _selectedDate.toLocal().month.toString() +
        "-" +
        _selectedDate.toLocal().day.toString();

    if (_selectedTime.hour < 10) {
      jamKurang10 = "0" + _selectedTime.hour.toString();
      if (_selectedTime.minute < 10) {
        menitKurang10 = "0" + _selectedTime.minute.toString();
        _selectingTime.text = jamKurang10 + ":" + menitKurang10 + ":00";
      } else {
        _selectingTime.text =
            jamKurang10 + ":" + _selectedTime.minute.toString() + ":00";
      }
    } else if (_selectedTime.minute < 10) {
      menitKurang10 = "0" + _selectedTime.minute.toString();
      _selectingTime.text =
          _selectedTime.hour.toString() + ":" + menitKurang10 + ":00";
    } else {
      _selectingTime.text = _selectedTime.hour.toString() +
          ":" +
          _selectedTime.minute.toString() +
          ":00";
    }
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getID();
    print("token" + token + "\n device" + device);
  }

  getKeperluan() {
    networkOjol.getKeperluan().then((response) {
      if (response.result == "true") {
        print(
            "Contoh Hasil : " + response.data[0]?.idKeperluan.toString() ?? "");
        setState(() {
          dataKeperluan = response?.data ?? "";
        });
      } else {
        print("Tidak Ada Hasil");
        setState(() {
          dataKeperluan = null;
        });
      }
    });
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _selectedDate, // Refer step 1
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 5),
    );
    if (picked != null && picked != _selectedDate)
      setState(() {
        _selectedDate = picked;
        _selectingDate.text = _selectedDate.toLocal().year.toString() +
            "-" +
            _selectedDate.toLocal().month.toString() +
            "-" +
            _selectedDate.toLocal().day.toString();
      });
  }

  _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
    );
    if (picked != null && picked != _selectedTime)
      setState(() {
        _selectedTime = picked;
        if (_selectedTime.hour < 10) {
          jamKurang10 = "0" + _selectedTime.hour.toString();
          if (_selectedTime.minute < 10) {
            menitKurang10 = "0" + _selectedTime.minute.toString();
            _selectingTime.text = jamKurang10 + ":" + menitKurang10 + ":00";
          } else {
            _selectingTime.text =
                jamKurang10 + ":" + _selectedTime.minute.toString() + ":00";
          }
        } else if (_selectedTime.minute < 10) {
          menitKurang10 = "0" + _selectedTime.minute.toString();
          _selectingTime.text =
              _selectedTime.hour.toString() + ":" + menitKurang10 + ":00";
        } else {
          _selectingTime.text = _selectedTime.hour.toString() +
              ":" +
              _selectedTime.minute.toString() +
              ":00";
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    CameraPosition initCamPos = CameraPosition(
        zoom: CAMERA_ZOOM,
        tilt: CAMERA_TILT,
        bearing: CAMERA_BEARING,
        target: currLoc != null
            ? LatLng(currLoc.latitude, currLoc.longitude)
            : LatLng(-6.2462238, 106.8768418));
    return Scaffold(
      body: Column(
        children: [
          Expanded(
              child: Stack(
            children: [
              GoogleMap(
                  mapType: MapType.normal,
                  markers: _markers,
                  polylines: polylines,
                  tiltGesturesEnabled: true,
                  myLocationEnabled: true,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                    moveToCurrentLocation();
                  },
                  onCameraIdle: () async {
                    print("Camera Idle");
                    if (isSelectOrigin) {
                      oriAddress = await getCurrentAddress();
                      oriLatLang = LocationData.fromMap({
                        "latitude": currLoc.latitude,
                        "longitude": currLoc.longitude
                      });
                    }
                    if (isSelectDest) {
                      destAddress = await getCurrentAddress();
                      destLatLang = LocationData.fromMap({
                        "latitude": currLoc.latitude,
                        "longitude": currLoc.longitude
                      });
                    }
                  },
                  onCameraMove: (CameraPosition position) {
                    currLoc = LocationData.fromMap({
                      "latitude": position.target.latitude,
                      "longitude": position.target.longitude
                    });
                    if (isShowPinMarker) {
                      print("Marker Jalan");
                      var pinPosition = LatLng(
                          position.target.latitude, position.target.longitude);
                      setState(() {
                        _markers.removeWhere(
                            (m) => m.markerId.value == "pinMarker");
                        _markers.add(Marker(
                            markerId: MarkerId("pinMarker"),
                            position: pinPosition,
                            icon: pinIcon));
                      });
                    }
                  },
                  initialCameraPosition: initCamPos),
              isSelectOrigin || isSelectDest
                  ? Container(
                      padding: EdgeInsets.all(16),
                      width: MediaQuery.of(context).size.width,
                      child: SearchMapPlaceWidget(
                        apiKey: apiKey,
                        onSelected: (Place place) async {
                          final geolocation = await place.geolocation;
                          currLoc = LocationData.fromMap({
                            "latitude": geolocation.coordinates.latitude,
                            "longitude": geolocation.coordinates.longitude
                          });
                          moveToCurrentLocation();
                        },
                      ),
                    )
                  : Container(),
              isSelectOrigin || isSelectDest
                  ? Positioned(
                      bottom: 0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.transparent,
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: BorderSide(color: Colors.red[700])),
                          onPressed: () async {
                            if (isSelectOrigin) {
                              setState(() {
                                isSelectOrigin = false;
                                isSelectDest = true;
                                setOriginMarker();
                              });
                            } else if (isSelectDest) {
                              distance = await countDistance();
                              cost = await countCost();
                              duration = countDuration();
                              setState(() {
                                isSelectDest = false;
                                isShowPinMarker = false;
                                isReviewRouteBeforeOrder = true;
                                deleteMarkerbyID("pin");
                                setDestinationMarker();
                                setPolylineOrder();
                              });
                            }
                          },
                          color: Colors.red[700],
                          textColor: Colors.white,
                          elevation: 0,
                          child: Text(isSelectOrigin
                              ? "SET LOKASI JEMPUT"
                              : "SET LOKASI TUJUAN"),
                        ),
                      ))
                  : Container(),
              isReadyToCreateNewOrder
                  ? Positioned(
                      bottom: 0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.transparent,
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: BorderSide(color: Colors.red[700])),
                          onPressed: () => createNewOrder(),
                          color: Colors.red[700],
                          textColor: Colors.white,
                          elevation: 0,
                          child: Text("BUAT ORDER BARU"),
                        ),
                      ))
                  : Container(),
              isReviewRouteBeforeOrder
                  ? Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16),
                            child: FloatingActionButton(
                              onPressed: () => clearOrder(),
                              child: Icon(
                                Icons.close,
                                color: Colors.white,
                              ),
                              mini: true,
                              backgroundColor: Colors.red[700],
                              elevation: 1,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              padding: EdgeInsets.all(16),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 10,
                                    ),
                                    child: TabBar(
                                      controller: controller,
                                      tabs: <Widget>[
                                        Tab(
                                          text: "Order Now",
                                        ),
                                      ],
                                      indicatorColor: Colors.red[700],
                                      labelColor: Colors.red[700],
                                    ),
                                  ),
                                  Container(
                                    height: 250,
                                    child: TabBarView(
                                        controller: controller,
                                        children: <Widget>[
                                          Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Icon(Icons.trip_origin,
                                                      size: 16,
                                                      color: Colors.green),
                                                  SizedBox(width: 8),
                                                  Expanded(
                                                      child: Text(
                                                          oriAddress ?? "",
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .ellipsis))
                                                ],
                                              ),
                                              Divider(
                                                height: 10,
                                              ),
                                              Row(
                                                children: [
                                                  Icon(Icons.place,
                                                      size: 16,
                                                      color: Colors.red[700]),
                                                  SizedBox(width: 8),
                                                  Expanded(
                                                      child: Text(
                                                          destAddress ?? "",
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .ellipsis))
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Text("Keperluan"),
                                                  ),
                                                  Expanded(
                                                    child:
                                                        DropdownButton<String>(
                                                      value: keperluan,
                                                      icon: Icon(Icons
                                                          .keyboard_arrow_down),
                                                      iconSize: 20,
                                                      elevation: 8,
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          keperluan = newValue;
                                                        });
                                                      },
                                                      items: dataKeperluan
                                                          .map((item) {
                                                        return DropdownMenuItem(
                                                          child: Text(item
                                                              .namaKeperluan),
                                                          value: item
                                                              .namaKeperluan,
                                                        );
                                                      }).toList(),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                        "Jumlah Penumpang"),
                                                  ),
                                                  Expanded(
                                                    child: DropdownButton<int>(
                                                      value: jumlahPenumpang,
                                                      icon: Icon(Icons
                                                          .keyboard_arrow_down),
                                                      iconSize: 20,
                                                      elevation: 8,
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                      onChanged:
                                                          (int newValue) {
                                                        setState(() {
                                                          jumlahPenumpang =
                                                              newValue;
                                                        });
                                                      },
                                                      items: <int>[
                                                        1,
                                                        2,
                                                        3,
                                                        4
                                                      ].map<
                                                              DropdownMenuItem<
                                                                  int>>(
                                                          (int value) {
                                                        return DropdownMenuItem<
                                                            int>(
                                                          value: value,
                                                          child: Text(
                                                              value.toString()),
                                                        );
                                                      }).toList(),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              TextField(
                                                controller: _catatan,
                                                decoration: InputDecoration(
                                                    focusedBorder:
                                                        UnderlineInputBorder(
                                                            borderSide:
                                                                BorderSide(
                                                                    color: Colors
                                                                            .red[
                                                                        700])),
                                                    hintText:
                                                        "Catatan untuk Order Now"),
                                              ),
                                            ],
                                          ),
                                        ]),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.transparent,
                            padding: EdgeInsets.all(8),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  side: BorderSide(color: Colors.red[700])),
                              onPressed: () {
                                setState(() {
                                  if (orderType == 0 || orderType == 1) {
                                    _newSchedule = hariNow + " " + jamNow;
                                  } else if (orderType == 2) {
                                    _newSchedule = _selectingDate.text +
                                        " " +
                                        _selectingTime.text;
                                  }

                                  if (_catatan.text == "") {
                                    catatanFinal = "-";
                                  } else {
                                    catatanFinal = _catatan.text;
                                  }
                                });

                                print(_newSchedule);
                                print(catatanFinal);

                                if (orderType == 0) {
                                  setState(() {
                                    orderType = 1;
                                  });
                                  // print(
                                  //     "idOrderType : " + orderType.toString());
                                  insertOrderNew(_newSchedule, catatanFinal);
                                } else if (orderType == 1) {
                                  // print(
                                  //     "idOrderType : " + orderType.toString());
                                  insertOrderNew(_newSchedule, catatanFinal);
                                } else if (orderType == 2) {
                                  // print(
                                  //     "idOrderType : " + orderType.toString());
                                  // insertOrder(_newSchedule, catatanFinal);
                                }
                              },
                              color: Colors.red[700],
                              textColor: Colors.white,
                              elevation: 0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                      "Total Jarak : (${distance.toStringAsFixed(2)} Km - ${duration.toStringAsFixed(2)} Jam)"),
                                  Text("ORDER")
                                  // Text(rupiah(cost)),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  : Container(),
            ],
          ))
        ],
      ),
    );
  }

  //Untuk Menampilkan Button Order
  void createNewOrder() async {
    oriAddress = await getCurrentAddress();
    oriLatLang = LocationData.fromMap(
        {"latitude": currLoc.latitude, "longitude": currLoc.longitude});
    destLatLang = null;
    destAddress = null;
    isReadyToCreateNewOrder = false;
    polylines.clear();
    _markers.removeWhere((m) => m.markerId.value == "originMarker");
    _markers.removeWhere((m) => m.markerId.value == "destinationMarker");
    isSelectOrigin = true;
    isShowPinMarker = true;
    moveToCurrentLocation();
  }

  //Untuk Clear Input Order
  void clearOrder() {
    setState(() {
      oriAddress = null;
      oriLatLang = null;
      destLatLang = null;
      destAddress = null;
      isReviewRouteBeforeOrder = false;
      isReadyToCreateNewOrder = true;
      polylines.clear();
      _markers.removeWhere((m) => m.markerId.value == "originMarker");
      _markers.removeWhere((m) => m.markerId.value == "destinationMarker");
      isSelectOrigin = false;
      isShowPinMarker = false;
    });
  }

  //Untuk Membuat Garis Rute
  void setPolylineOrder() async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        apiKey,
        PointLatLng(oriLatLang.latitude, oriLatLang.longitude),
        PointLatLng(destLatLang.latitude, destLatLang.longitude));

    polylineCoordinates.clear();
    polylines.removeWhere((p) => p.polylineId.value == "orderRoute");
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });

      setState(() {
        polylines.add(Polyline(
            width: 5,
            polylineId: PolylineId("orderRoute"),
            color: Color.fromARGB(255, 40, 122, 198),
            points: polylineCoordinates));
      });
    }
  }

  // Untuk Menghitung Harga
  countCost() async {
    int cost = 9000;
    double distance = await countDistance();
    if (distance > 5) {
      print("jaraknya" + distance.round().toString());
      print(distance.ceil());
      int additionalCost = (distance.round() - 5) * 3000;
      cost += additionalCost;
    }
    return cost;
  }

  //Untuk Menghitung Jarak
  countDistance() async {
    double distance = await Geolocator().distanceBetween(
      oriLatLang.latitude,
      oriLatLang.longitude,
      destLatLang.latitude,
      destLatLang.longitude,
    );
    if (distance < 300000) {
      return distance / 1000;
    } else {
      return Toast.show("Kelebihan !", context);
    }
  }

  //Untuk Menghitung Estimasi Waktu
  countDuration() {
    duration = distance / 15;
    return duration;
  }

  void setOriginMarker() async {
    // _markers.removeWhere((m) => m.markerId.value == "originMarker");
    _markers.add(Marker(
        markerId: MarkerId("originMarker"),
        position: LatLng(oriLatLang.latitude, oriLatLang.longitude),
        icon: oriLocIcon));
  }

  void setDestinationMarker() async {
    // _markers.removeWhere((m) => m.markerId.value == "destinationMarker");
    _markers.add(Marker(
        markerId: MarkerId("destinationMarker"),
        position: LatLng(destLatLang.latitude, destLatLang.longitude),
        icon: destLocIcon));
  }

  Future<void> setMarkerIcon() async {
    oriLocIcon =
        await getBitmapDescriptorFromAssetBytes("images/marker_start.png", 100);
    destLocIcon = await getBitmapDescriptorFromAssetBytes(
        "images/marker_destination.png", 100);
    pinIcon =
        await getBitmapDescriptorFromAssetBytes("images/marker_pin.png", 100);
  }

  Future<void> setInitialLocation() async {
    currLoc = await loc.getLocation();
    oriLatLang = await loc.getLocation();
  }

  Future<void> moveToCurrentLocation({LocationData locData}) async {
    if (currLoc == null) currLoc = await loc.getLocation();
    if (locData == null) locData = currLoc;
    CameraPosition campos = CameraPosition(
        zoom: CAMERA_ZOOM, target: LatLng(locData.latitude, locData.longitude));

    GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(campos));

    if (isShowPinMarker) {
      setState(() {
        var pinPosition = LatLng(locData.latitude, locData.longitude);
        deleteMarkerbyID("pin");
        _markers.add(Marker(
            markerId: MarkerId("pin"), position: pinPosition, icon: pinIcon));
      });
    }
  }

  void deleteMarkerbyID(String s) {
    _markers.removeWhere((element) => element.markerId.value == s);
  }

  Future<String> getCurrentAddress() async {
    final coordinates = Coordinates(currLoc.latitude, currLoc.longitude);
    var address =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstLoc = address.first;
    return firstLoc.addressLine;
  }

  //New Insert Order
  void insertOrderNew(
    String dateSchedule,
    String catatanFinal,
  ) {
    networkOjol
        .insertOrderNew(
      orderStatus.toString(),
      orderType.toString(),
      iduser.toString(),
      jumlahPenumpang.toString(),
      keperluan,
      oriLatLang.latitude.toString(),
      oriLatLang.longitude.toString(),
      oriAddress,
      destLatLang.latitude.toString(),
      destLatLang.longitude.toString(),
      destAddress,
      distance.toStringAsFixed(2),
      duration.toStringAsFixed(2),
      catatanFinal,
      dateSchedule,
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        if (orderType == 1) {
          print(response.result.toString() + response.message.toString());
          Navigator.pushReplacementNamed(context, DetailDriverScreen.tag,
              arguments: response.data.idOrder.toString());
          Toast.show(response.message, context);
          print("Berhasil Menemukan Driver");
        } else if (orderType == 2) {}
      } else {
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        Toast.show(response.message, context);
      }
    });
    print("origin :" +
        oriLatLang.latitude.toString() +
        "," +
        oriLatLang.longitude.toString() +
        "\ndestination" +
        destLatLang.latitude.toString() +
        "," +
        destLatLang.longitude.toString());
  }

  void insertOrder(
    String dateSchedule,
    String catatanFinal,
  ) {
    networkOjol
        .insertOrder(
      orderStatus.toString(),
      orderType.toString(),
      iduser.toString(),
      jumlahPenumpang.toString(),
      keperluan,
      oriLatLang.latitude.toString(),
      oriLatLang.longitude.toString(),
      oriAddress,
      destLatLang.latitude.toString(),
      destLatLang.longitude.toString(),
      destAddress,
      distance.toStringAsFixed(2),
      duration.toStringAsFixed(2),
      catatanFinal,
      dateSchedule,
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        if (orderType == 1) {
          Navigator.of(context).pushReplacementNamed(WaitingDriverScreen.tag,
              arguments: response.data.idOrder.toString());
        } else if (orderType == 2) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              UtamaScreen.tag, (Route<dynamic> route) => false);
        }
      } else {
        Toast.show(response.message, context);
      }
    });
    print("origin :" +
        oriLatLang.latitude.toString() +
        "," +
        oriLatLang.longitude.toString() +
        "\ndestination" +
        destLatLang.latitude.toString() +
        "," +
        destLatLang.longitude.toString());
  }
}

import 'package:vmscustomer/screens/utama_screen.dart';
import 'package:flutter/material.dart';

class MyHome extends StatefulWidget {
  static String tag = 'home-page';
  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Home"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Center(
                child: Image.asset(
              "images/logo-site.png",
              width: 300,
              height: 215,
            )),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(
              "Hutama Karya merupakan Badan Usaha Milik Negara (BUMN) yang bergerak dibidang jasa konstruksi, pengembangan dan penyedia jasa jalan tol.",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 10),
              child: Center(
                child: RaisedButton(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  onPressed: () {
                    Navigator.of(context).pushNamed(UtamaScreen.tag);
                  },
                  child: Text(
                    "Get Started",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ))
        ],
      ),
    );
  }
}

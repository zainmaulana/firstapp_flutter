import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_orderchat.dart';
import 'package:vmscustomer/models/model_ordergetdetail.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/chatroom_screen.dart';
import 'package:vmscustomer/screens/detaildriver_screen.dart';
import 'package:vmscustomer/screens/utama_screen.dart';

class DetailOrderScreen extends StatefulWidget {
  static String tag = 'detailorder-page';
  DataDetailOrder dataDetailOrder;
  DetailOrderScreen({Key key, this.dataDetailOrder}) : super(key: key);

  @override
  _DetailOrderScreenState createState() => _DetailOrderScreenState();
}

class _DetailOrderScreenState extends State<DetailOrderScreen> {
  int iduser;
  String token, device;
  NetworkOjol networkOjol = NetworkOjol();
  final TextEditingController _cancelComment = TextEditingController();
  final TextEditingController _selectingDate = TextEditingController();
  final TextEditingController _selectingTime = TextEditingController();
  String _newSchedule;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  String jamKurang10;
  String menitKurang10;
  Timer _timer;
  int _start = 10;

  int startDateH;
  int startDateM;
  TimeOfDay _selectedTimeLimit;
  int totalNow;
  int totalStartDate;
  int totalStartDateY;
  int totalStartDateM;
  int totalStartDateD;

  DataChatOrder dataChatOrder;

  List<int> rotatedBox = [1, 2, 3, 4];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
    startTimer();

    _selectedDate = DateTime.now();
    _selectedTime = TimeOfDay.now();

    _selectingDate.text = _selectedDate.toLocal().year.toString() +
        "-" +
        _selectedDate.toLocal().month.toString() +
        "-" +
        _selectedDate.toLocal().day.toString();

    if (_selectedTime.hour < 10) {
      jamKurang10 = "0" + _selectedTime.hour.toString();
      if (_selectedTime.minute < 10) {
        menitKurang10 = "0" + _selectedTime.minute.toString();
        _selectingTime.text = jamKurang10 + ":" + menitKurang10 + ":00";
      } else {
        _selectingTime.text =
            jamKurang10 + ":" + _selectedTime.minute.toString() + ":00";
      }
    } else if (_selectedTime.minute < 10) {
      menitKurang10 = "0" + _selectedTime.minute.toString();
      _selectingTime.text =
          _selectedTime.hour.toString() + ":" + menitKurang10 + ":00";
    } else {
      _selectingTime.text = _selectedTime.hour.toString() +
          ":" +
          _selectedTime.minute.toString() +
          ":00";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("My Order"),
      ),
      body: Container(
        child: tampilanDetailOrder(),
      ),
    );
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getID();
    print("ID User : " + iduser.toString());
  }

  Future<void> setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setInt("idorder", widget.dataDetailOrder.idOrder);
    print("ID Order : " + widget.dataDetailOrder.idOrder.toString());
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start > 1) {
            _selectedTimeLimit = TimeOfDay.now();

            checkStartOrder();
            getChats();
            displayOrder();

            print("Refresh State");
          }
        },
      ),
    );
  }

  void displayOrder() {
    startDateH = int.parse(widget.dataDetailOrder.startDate.substring(11, 13));
    startDateM = int.parse(widget.dataDetailOrder.startDate.substring(14, 16));
    startDateM = startDateM + 1; //Tambahan Waktu 15 Menit

    totalNow = _selectedDate.year + _selectedDate.month + _selectedDate.day;
    totalStartDateY =
        int.parse(widget.dataDetailOrder.startDate.substring(0, 4));
    totalStartDateM =
        int.parse(widget.dataDetailOrder.startDate.substring(5, 7));
    totalStartDateD =
        int.parse(widget.dataDetailOrder.startDate.substring(8, 10));
    totalStartDate = totalStartDateY + totalStartDateM + totalStartDateD;

    // print("ID Order : " + widget.dataDetailOrder.idOrder.toString());
    // print("Start Day : " + widget.dataDetailOrder.startDate.substring(0, 13));
    // print("Hari Sekarang : " + _selectedDate.toString().substring(0, 13));
    // print("Start Hour : " + startDateH.toString() + startDateM.toString());
    // print("Waktu Hour : " + _selectedTimeLimit.hour.toString());
    // print("Waktu Menit : " + _selectedTimeLimit.minute.toString());
    print("Total Now : " + totalNow.toString());
    print("Total StartDate : " + totalStartDate.toString());

    //Cek Batas Waktu
    if (totalStartDate == totalNow) {
      if (startDateH == _selectedTimeLimit.hour) {
        if (startDateM < _selectedTimeLimit.minute) {
          setState(() {
            cancelOrderAuto();
          });
        }
      }
    } else if (totalStartDate < totalNow) {
      setState(() {
        cancelOrderAuto();
      });
    }
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _selectedDate, // Refer step 1
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 5),
    );
    if (picked != null && picked != _selectedDate)
      setState(() {
        _selectedDate = picked;
        _selectingDate.text = _selectedDate.toLocal().year.toString() +
            "-" +
            _selectedDate.toLocal().month.toString() +
            "-" +
            _selectedDate.toLocal().day.toString();
      });
  }

  _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
    );
    if (picked != null && picked != _selectedTime)
      setState(() {
        _selectedTime = picked;
        if (_selectedTime.hour < 10) {
          jamKurang10 = "0" + _selectedTime.hour.toString();
          if (_selectedTime.minute < 10) {
            menitKurang10 = "0" + _selectedTime.minute.toString();
            _selectingTime.text = jamKurang10 + ":" + menitKurang10 + ":00";
          } else {
            _selectingTime.text =
                jamKurang10 + ":" + _selectedTime.minute.toString() + ":00";
          }
        } else if (_selectedTime.minute < 10) {
          menitKurang10 = "0" + _selectedTime.minute.toString();
          _selectingTime.text =
              _selectedTime.hour.toString() + ":" + menitKurang10 + ":00";
        } else {
          _selectingTime.text = _selectedTime.hour.toString() +
              ":" +
              _selectedTime.minute.toString() +
              ":00";
        }
      });
  }

  changeScheduleDialog(BuildContext context) {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("Konfirmasi Ganti Jadwal"),
          content: SingleChildScrollView(
            child: Column(
              children: [
                myTextField(
                  _selectingDate,
                  TextInputType.text,
                  null,
                  null,
                  Icon(Icons.calendar_today),
                  IconButton(
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                    ),
                    onPressed: () {
                      _selectDate(context);
                    },
                  ),
                  true,
                ),
                myTextField(
                  _selectingTime,
                  TextInputType.text,
                  null,
                  null,
                  Icon(Icons.access_time),
                  IconButton(
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                    ),
                    onPressed: () {
                      _selectTime(context);
                    },
                  ),
                  true,
                ),
                // ListTile(
                //   title: Text(
                //       "Tanggal: ${_selectedDate.year}-${_selectedDate.month}-${_selectedDate.day}"),
                //   trailing: Icon(Icons.calendar_today),
                //   onTap: _selectDate(context),
                // ),
                // ListTile(
                //   title: Text(
                //       "Waktu: ${_selectedTime.hour}:${_selectedTime.minute}"),
                //   trailing: Icon(Icons.access_time),
                //   onTap: _selectTime(context),
                // ),
              ],
            ),
          ),
          actions: [
            new TextButton(
              child: new Text(
                "Tutup",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new TextButton(
              child: new Text(
                "Change",
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                setState(() {
                  _newSchedule =
                      _selectingDate.text + " " + _selectingTime.text;
                });
                print(_newSchedule);
                changeOrder();
              },
            ),
          ],
        ));
  }

  cancelOrderDialog(BuildContext context) {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("Konfirmasi Batal Order"),
          content: myTextField(
            _cancelComment,
            TextInputType.multiline,
            null,
            null,
            null,
            null,
            false,
          ),
          actions: [
            new TextButton(
              child: new Text(
                "Tutup",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new TextButton(
              child: new Text(
                "Cancel",
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                cancelOrder();
              },
            ),
          ],
        ));
  }

  void changeOrder() {
    networkOjol
        .changeOrder(
      widget.dataDetailOrder.idOrder.toString(),
      _newSchedule,
    )
        .then((response) {
      if (response.result == "true") {
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        Toast.show(response.message, context);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void cancelOrder() {
    networkOjol
        .cancelOrder(
      widget.dataDetailOrder.idOrder.toString(),
      _cancelComment.text,
    )
        .then((response) {
      if (response.result == "true") {
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        Toast.show(response.message, context);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void cancelOrderAuto() {
    networkOjol
        .cancelOrder(
      widget.dataDetailOrder.idOrder.toString(),
      "Dibatalkan karena Tidak ada Konfirmasi/Terlambat",
    )
        .then((response) {
      if (response.result == "true") {
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        Toast.show(response.message, context);
        _timer.cancel();
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  void checkStartOrder() {
    networkOjol
        .checkStartOrder(widget.dataDetailOrder.idOrder.toString())
        .then((response) {
      print("ID Order : " + widget.dataDetailOrder.idOrder.toString());
      if (response.result == "true") {
        print(response.result.toString() + response.message.toString());
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
          DetailDriverScreen.tag,
          (Route<dynamic> route) => false,
          arguments: response.data.idOrder.toString(),
        );
        // Navigator.pushReplacementNamed(context, DetailDriverScreen.tag,
        //     arguments: response.data.idOrder.toString());
        print("Perjalanan Dimulai");
        _timer.cancel();
      } else {
        print("Perjalanan Belum Dimulai");
      }
    });
  }

  void getChats() {
    networkOjol
        .getChats(widget.dataDetailOrder.idOrder.toString())
        .then((response) {
      if (response.result == "true") {
        print("Nama Driver : " + response.data[0].namaDriver);
        setState(() {
          dataChatOrder = response.data[0];
        });
      } else {
        print("Tidak Ada Hasil");
        setState(() {
          dataChatOrder = null;
        });
      }
    });
  }

  Widget myTextField(
    TextEditingController controller,
    TextInputType type,
    String hint,
    String label,
    Icon prefix,
    IconButton suffix,
    bool readOnly,
  ) {
    return TextField(
      style: TextStyle(
        color: Colors.black,
      ),
      controller: controller,
      keyboardType: type,
      readOnly: readOnly,
      decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.red[700],
          )),
          prefixIcon: prefix,
          suffixIcon: suffix,
          hintText: hint,
          hintStyle: TextStyle(color: Colors.black),
          labelText: label,
          labelStyle: TextStyle(color: Colors.black)),
    );
  }

  Widget tampilanDetailOrder() {
    return Column(
      children: [
        Container(
          height: 150,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hi, " +
                      widget.dataDetailOrder.namaKaryawan +
                      "! This is your report order",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.red[700],
                  ),
                ),
                Text(
                  "Make sure to contact the driver to confirm your order's origin and destination",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
                Text(
                  "Please read Important Notice for more information",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
          child: Flexible(
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Order No #" + widget.dataDetailOrder?.orderCode ?? "Kosong",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey[700],
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 300,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Icon(
                      Icons.trip_origin,
                      size: 30,
                      color: Colors.red[700],
                    ),
                  ),
                  for (var i in rotatedBox)
                    RotatedBox(
                      quarterTurns: 1,
                      child: Icon(
                        Icons.remove,
                        size: 30,
                        color: Colors.red[700],
                      ),
                    ),
                  Flexible(
                    child: Icon(
                      Icons.place,
                      size: 30,
                      color: Colors.green,
                    ),
                  ),
                ],
              )),
              Expanded(
                flex: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Flexible(
                        child: Text(
                          widget.dataDetailOrder?.pickupLocation ?? "Kosong",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.watch_later_outlined,
                          size: 20,
                          color: Colors.red[700],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: Text(
                            widget.dataDetailOrder?.startDate ?? "Kosong",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.group,
                          size: 20,
                          color: Colors.red[700],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: Text(
                            widget.dataDetailOrder?.jumlahPenumpang
                                    .toString() ??
                                "Kosong",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.notes,
                          size: 20,
                          color: Colors.red[700],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: Text(
                            widget.dataDetailOrder?.keperluan ?? "Kosong",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: Flexible(
                        child: Text(
                          widget.dataDetailOrder?.dropLocation ?? "Kosong",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Flexible(
                        child: Text(
                          "Catatan : " + widget.dataDetailOrder?.catatan ??
                              "Kosong",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 8),
          child: Flexible(
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Driver Information",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey[700],
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        Flexible(
          child: Container(
            height: 100,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 35,
                        backgroundColor: Colors.grey[400],
                        backgroundImage: NetworkImage(
                            "https://vms.hutamakarya.com/image_driver/" +
                                widget.dataDetailOrder.imageDriver),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 40),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.dataDetailOrder?.namaDriver ?? "Kosong",
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              widget.dataDetailOrder?.namaUnit ?? "Kosong",
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              widget.dataDetailOrder?.nopolUnit ?? "Kosong",
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 40),
                        child: IconButton(
                          icon: Icon(
                            Icons.message,
                            color: Colors.red[700],
                            size: 40,
                          ),
                          onPressed: () {
                            setPref();
                            Navigator.of(context).pushNamed(ChatRoom.tag,
                                arguments: dataChatOrder);
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 5),
          child: MaterialButton(
            minWidth: double.infinity,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
                side: BorderSide(color: Colors.red[700])),
            color: Colors.red[700],
            textColor: Colors.white,
            padding: EdgeInsets.all(10),
            onPressed: () {
              changeScheduleDialog(context);
            },
            child: Text(
              "Change Schedule",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 5),
          child: MaterialButton(
            minWidth: double.infinity,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
                side: BorderSide(color: Colors.red[700])),
            color: Colors.grey[200],
            textColor: Colors.red[700],
            padding: EdgeInsets.all(10),
            onPressed: () {
              cancelOrderDialog(context);
            },
            child: Text(
              "Cancel Order",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }
}

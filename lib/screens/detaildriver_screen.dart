import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/pin_pill_info.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/screens/utama_screen.dart';
import 'package:vmscustomer/widgets/map_pin_pill.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'dart:async';

const double CAMERA_ZOOM = 16;
const double CAMERA_TILT = 80;
const double CAMERA_BEARING = 30;
const LatLng SOURCE_LOCATION = LatLng(-6.2969516, 106.6962871);
const LatLng DEST_LOCATION = LatLng(-6.296995, 106.7105706);

class DetailDriverScreen extends StatefulWidget {
  static String tag = "detaildriver-page";
  String idOrder;
  DetailDriverScreen({Key key, this.idOrder = null}) : super(key: key);

  @override
  _DetailDriverScreenState createState() => _DetailDriverScreenState();
}

class _DetailDriverScreenState extends State<DetailDriverScreen>
    with AutomaticKeepAliveClientMixin {
  Timer _timer;
  int _start = 10;
  NetworkOjol networkOjol = NetworkOjol();

  String latDriver;
  String lngDriver;
  String imageDriver;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = Set<Marker>();
// for my drawn routes on the map
  Set<Polyline> _polylines = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints;
  String googleAPIKey =
      'AIzaSyB87Z7EaGa873QQ6RrF5QaAA5E42pxu-jI'; //AIzaSyA961qUUYeU2tnBuk4gS1fpiXVjnCFnbcQ
// for my custom marker pins
  BitmapDescriptor sourceIcon;
  BitmapDescriptor destinationIcon;
// the user's initial location and current location
// as it moves
  LocationData currentLocation;
// a reference to the destination location
  LocationData destinationLocation;
// wrapper around the location API
  Location location;
  double pinPillPosition = -150;
  PinInformation currentlySelectedPin = PinInformation(
    pinPath: '',
    avatarPath: '',
    location: LatLng(0, 0),
    locationName: '',
    labelColor: Colors.grey,
  );
  PinInformation sourcePinInfo;
  PinInformation destinationPinInfo;

  String originAdress;
  String destAddress;
  String idOrder, idOrderStatus, orderCode;

  String namaDriver;
  String namaKaryawan;
  String unitKendaraan;
  String nopolUnit;

  String startDate;
  int startDateH;
  int startDateM;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  int totalNow;
  int totalStartDate;
  int totalStartDateY;
  int totalStartDateM;
  int totalStartDateD;

  final TextEditingController _cancelComment = TextEditingController();
  String komentarOrder;

  @override
  void initState() {
    super.initState();

    // create an instance of Location
    location = new Location();
    polylinePoints = PolylinePoints();

    // subscribe to changes in the user's location
    // by "listening" to the location's onLocationChanged event
    location.onLocationChanged.listen((LocationData cLoc) {
      // cLoc contains the lat and long of the
      // current user's position in real time,
      // so we're holding on to it
      currentLocation = cLoc;
      updatePinOnMap();
    });
    // set custom marker pins
    setSourceAndDestinationIcons();
    // set the initial location
    startTimer();
  }

  checkLokasiDriver() {
    networkOjol.getDriver(widget.idOrder).then((response) {
      print("idLokasi :" + response.data.idLokasiDriver.toString());
      if (response.data.idOrderStatus == 1 ||
          response.data.idOrderStatus == 2 ||
          response.data.idOrderStatus == 3) {
        // Toast.show(res.msg, context);
        // Navigator.popAndPushNamed(context, DetailDriverScreen.id);
        // _timer.cancel();

        setState(() {
          latDriver = response.data.lastLatitude;
          lngDriver = response.data.lastLongitude;
          imageDriver = "https://vms.hutamakarya.com/image_driver/" +
              response.data.imageDriver;
          namaDriver = response.data.namaDriver;
          namaKaryawan = response.data.namaKaryawan;
          unitKendaraan = response.data.namaUnit;
          nopolUnit = response.data.nopolUnit;
          idOrder = response.data.idOrder.toString();
          idOrderStatus = response.data.idOrderStatus.toString();
          startDate = response.data.startDate;
          // orderCode = response.data.orderCode;
        });
        print("Order Berjalan");
        showPinsOnMap();
        setStateCustomer();

        // print("laat:" + latDriver + "\nlong:" + lngDriver);
      } else if (response.data.idOrderStatus == 4 ||
          response.data.idOrderStatus == 5) {
        Toast.show(response.message, context);
        print("Order Tidak Berjalan/Sudah Selesai");
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
      }
    });
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 3);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start > 1) {
            // timer.cancel();

            _selectedDate = DateTime.now();
            _selectedTime = TimeOfDay.now();

            checkLokasiDriver();
            print("Refresh State");
            setInitialLocation();
          }
          // else {
          //   _start = _start - 1;
          // }
        },
      ),
    );
  }

  void setSourceAndDestinationIcons() async {
    sourceIcon = await getBitmapDescriptorFromAssetBytes(
      "images/marker_start.png",
      100,
    );
    destinationIcon = await getBitmapDescriptorFromAssetBytes(
      "images/marker_destination.png",
      100,
    );
  }

  void setInitialLocation() async {
    // set the initial location by pulling the user's
    // current location from the location's getLocation()
    currentLocation = await location.getLocation();

    // hard-coded destination for this example
    destinationLocation = LocationData.fromMap({
      "latitude": double.parse(latDriver),
      "longitude": double.parse(lngDriver),
    });
    print("init masuk : " +
        LatLng(double.parse(latDriver), double.parse(lngDriver)).toString());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    CameraPosition initialCameraPosition = CameraPosition(
        zoom: CAMERA_ZOOM,
        tilt: CAMERA_TILT,
        bearing: CAMERA_BEARING,
        target: SOURCE_LOCATION);
    if (currentLocation != null) {
      initialCameraPosition = CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: CAMERA_ZOOM,
          tilt: CAMERA_TILT,
          bearing: CAMERA_BEARING);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Travel Page"),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
              myLocationEnabled: true,
              compassEnabled: true,
              tiltGesturesEnabled: false,
              markers: _markers,
              polylines: _polylines,
              mapType: MapType.normal,
              initialCameraPosition: initialCameraPosition,
              onTap: (LatLng loc) {
                pinPillPosition = -150;
              },
              onMapCreated: (GoogleMapController controller) {
                controller.setMapStyle(Utils.mapStyles);
                _controller.complete(controller);
                // my map has completed being created;
                // i'm ready to show the pins on the map
                showPinsOnMap();
              }),
          Positioned(
            top: 8,
            left: 60,
            right: 60,
            child: setStateCustomer(),
          ),
          sourcePinInfo == null
              ? Center(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    Text("Memuat Lokasi Anda dan Driver")
                  ],
                ))
              : Container(),
          // Positioned(
          //     top: 20,
          //     left: 50,
          //     right: 50,
          //     child: RaisedButton(onPressed: () {
          //       showPinsOnMap();
          //     })),
          MapPinPillComponent(
              pinPillPosition: pinPillPosition,
              currentlySelectedPin: currentlySelectedPin)
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget setStateCustomer() {
    if (idOrderStatus == "2") {
      startDateH = int.parse(startDate.substring(11, 13));
      startDateM = int.parse(startDate.substring(14, 16));
      startDateM = startDateM + 15; //Tambahan Waktu 15 Menit

      totalNow = _selectedDate.year + _selectedDate.month + _selectedDate.day;
      totalStartDateY = int.parse(startDate.substring(0, 4));
      totalStartDateM = int.parse(startDate.substring(5, 7));
      totalStartDateD = int.parse(startDate.substring(8, 10));
      totalStartDate = totalStartDateY + totalStartDateM + totalStartDateD;

      // print("ID Order : " + widget.dataDetailOrder.idOrder.toString());
      // print("Start Day : " + startDate.substring(0, 13));
      // print("Hari Sekarang : " + _selectedDate.toString().substring(0, 13));
      print("Start Hour : " + startDateH.toString() + startDateM.toString());
      print("Waktu Hour : " + _selectedTime.hour.toString());
      print("Waktu Menit : " + _selectedTime.minute.toString());
      // print("Total Now : " + totalNow.toString());
      // print("Total StartDate : " + totalStartDate.toString());

      if (totalStartDate == totalNow) {
        if (startDateH == _selectedTime.hour) {
          if (startDateM < _selectedTime.minute) {
            return RaisedButton(
              onPressed: () {
                cancelOrderDialog(context);
              },
              color: Colors.red[700],
              child: Text(
                "Cancel Order",
                style: TextStyle(color: Colors.white),
              ),
            );
          }
        } else if (startDateH < _selectedTime.hour) {
          return RaisedButton(
            onPressed: () {
              cancelOrderDialog(context);
            },
            color: Colors.red[700],
            child: Text(
              "Cancel Order",
              style: TextStyle(color: Colors.white),
            ),
          );
        }
      } else if (totalStartDate < totalNow) {
        return RaisedButton(
          onPressed: () {
            cancelOrderDialog(context);
          },
          color: Colors.red[700],
          child: Text(
            "Cancel Order",
            style: TextStyle(color: Colors.white),
          ),
        );
      }
    }
    return Container();
  }

  cancelOrderDialog(BuildContext context) {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("Konfirmasi Batal Order"),
          content: TextField(
            style: TextStyle(
              color: Colors.black,
            ),
            controller: _cancelComment,
            keyboardType: TextInputType.multiline,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                color: Colors.red[700],
              )),
            ),
          ),
          actions: [
            new TextButton(
              child: new Text(
                "Tutup",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new TextButton(
              child: new Text(
                "Cancel",
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                setState(() {
                  komentarOrder = _cancelComment.text;
                });
                batalOrder();
              },
            ),
          ],
        ));
  }

  void batalOrder() {
    print("ID Order : " + idOrder);
    print(komentarOrder);
    networkOjol
        .cancelOrder(
      idOrder,
      komentarOrder,
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
        _timer.cancel();
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  Future<void> showPinsOnMap() async {
    // get a LatLng for the source location
    // from the LocationData currentLocation object
    var pinPosition =
        LatLng(currentLocation.latitude, currentLocation.longitude);
    // get a LatLng out of the LocationData object
    var destPosition =
        LatLng(destinationLocation.latitude, destinationLocation.longitude);
    print("masuk");
    originAdress = await getCurrentAddress();
    sourcePinInfo = PinInformation(
      locationName: originAdress,
      location: SOURCE_LOCATION,
      pinPath: "https://vms.hutamakarya.com/image_asset/marker_start.png",
      avatarPath: "https://vms.hutamakarya.com/image_asset/vms-icon2.png",
      labelColor: Colors.red[700],
      name: namaKaryawan,
    );

    destAddress = await getDestAddress();
    destinationPinInfo = PinInformation(
      locationName: destAddress,
      location: LatLng(double.parse(latDriver), double.parse(lngDriver)),
      pinPath: "https://vms.hutamakarya.com/image_asset/marker_destination.png",
      avatarPath: imageDriver,
      labelColor: Colors.blue[900],
      name: namaDriver,
      unitKendaraan: unitKendaraan,
      nopolUnit: nopolUnit,
    );
    print("lat : " + latDriver + "\nlong : " + lngDriver);
    print(LatLng(double.parse(latDriver), double.parse(lngDriver)).toString());

    // add the initial source location pin
    _markers.add(Marker(
        markerId: MarkerId('sourcePin'),
        position: pinPosition,
        onTap: () {
          setState(() {
            currentlySelectedPin = sourcePinInfo;
            pinPillPosition = 0;
          });
        },
        icon: sourceIcon));
    // destination pin
    _markers.add(Marker(
        markerId: MarkerId('destPin'),
        position: destPosition,
        onTap: () {
          setState(() {
            currentlySelectedPin = destinationPinInfo;
            pinPillPosition = 80;
          });
        },
        icon: destinationIcon));
    // set the route lines on the map from source to destination
    // for more info follow this tutorial

    setPolylines();
  }

  void setPolylines() async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      googleAPIKey,
      PointLatLng(currentLocation.latitude, currentLocation.longitude),
      PointLatLng(destinationLocation.latitude, destinationLocation.longitude),
    );
    polylineCoordinates.clear();
    if (result != null) {
      print("Results not null");
      if (result.points.isNotEmpty) {
        result.points.forEach((PointLatLng point) {
          polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });

        setState(() {
          if (result != null) {
            print("Results not null");
            if (result.points.isNotEmpty) {
              _polylines.add(Polyline(
                  width: 2, // set the width of the polylines
                  polylineId: PolylineId("poly"),
                  color: Color.fromARGB(255, 40, 122, 198),
                  points: polylineCoordinates));
            }
          }
        });
      }
    }
  }

  void updatePinOnMap() async {
    // create a new CameraPosition instance
    // every time the location changes, so the camera
    // follows the pin as it moves with an animation
    CameraPosition cPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
    );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
    // do this inside the setState() so Flutter gets notified
    // that a widget update is due
    setState(() {
      // updated position
      var pinPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);

      sourcePinInfo.location = pinPosition;

      // the trick is to remove the marker (by id)
      // and add it again at the updated location
      _markers.removeWhere((m) => m.markerId.value == 'sourcePin');
      _markers.add(Marker(
          markerId: MarkerId('sourcePin'),
          onTap: () {
            setState(() {
              currentlySelectedPin = sourcePinInfo;
              pinPillPosition = 0;
            });
          },
          position: pinPosition, // updated position
          icon: sourceIcon));
    });
  }

  Future<String> getCurrentAddress() async {
    final coordinates =
        Coordinates(currentLocation.latitude, currentLocation.longitude);
    var address =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstLocation = address.first;
    return firstLocation.addressLine;
  }

  Future<String> getDestAddress() async {
    final coordinates = Coordinates(
        destinationLocation.latitude, destinationLocation.longitude);
    var address =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var firstLocation = address.first;
    return firstLocation.addressLine;
  }
}

class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';
}

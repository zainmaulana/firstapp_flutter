import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_orderhistory.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:vmscustomer/screens/utama_screen.dart';

class DetailHistory extends StatefulWidget {
  static String tag = "detailhistory-page";
  DataHistoryOrder dataHistoryOrder;
  DetailHistory({Key key, this.dataHistoryOrder}) : super(key: key);

  @override
  _DetailHistoryState createState() => _DetailHistoryState();
}

class _DetailHistoryState extends State<DetailHistory> {
  int iduser;
  String token, device;
  int nilai = 0;
  String commentRate;
  final TextEditingController _commentRating = TextEditingController();
  NetworkOjol networkOjol = NetworkOjol();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail " + widget.dataHistoryOrder?.orderCode ?? ""),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.trip_origin,
                  size: 30,
                  color: Colors.red[700],
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      widget.dataHistoryOrder?.pickupLocation ?? "",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.place,
                  size: 30,
                  color: Colors.green,
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      widget.dataHistoryOrder?.dropLocation ?? "",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Text("Jarak"),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Text(
                    " : " + widget.dataHistoryOrder?.realDistance ?? "" + " km",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Text("Durasi"),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Text(
                    " : " + widget.dataHistoryOrder?.realDuration ??
                        "" + " Jam",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Container(
            width: double.infinity,
            child: Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: tampilRating(),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getID();
    print("Token : " + token + "\n Device : " + device);
  }

  Widget tampilRating() {
    if (widget.dataHistoryOrder.idRating == null &&
        widget.dataHistoryOrder.idOrderStatus == 4) {
      return MaterialButton(
        minWidth: 200,
        height: 42,
        onPressed: () {
          tampilSubmit(context);
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: Colors.red[700],
        child: Text(
          "Nilai Order",
          style: TextStyle(color: Colors.white),
        ),
      );
    } else if (widget.dataHistoryOrder.idRating != null &&
        widget.dataHistoryOrder.idOrderStatus == 4) {
      return RatingBar(
        initialRating: widget.dataHistoryOrder.idRating.toDouble(),
        minRating: widget.dataHistoryOrder.idRating.toDouble(),
        direction: Axis.horizontal,
        itemCount: widget.dataHistoryOrder.idRating,
        ignoreGestures: true,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Colors.amber,
        ),
        onRatingUpdate: (rating) {
          print(rating);
          setState(() {
            nilai = rating.toInt();
          });
        },
      );
    }
    return null;
  }

  tampilSubmit(BuildContext context) {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("Rate & Comment"),
          content: SingleChildScrollView(
            child: Column(
              children: [
                RatingBar(
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    print(rating);
                    setState(() {
                      nilai = rating.toInt();
                    });
                  },
                ),
                TextField(
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  controller: _commentRating,
                  keyboardType: TextInputType.multiline,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.red[700],
                    )),
                    hintText: "Komentar untuk Order",
                  ),
                ),
              ],
            ),
          ),
          actions: [
            new TextButton(
              child: new Text(
                "Tutup",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new TextButton(
              child: new Text(
                "Submit",
                style: TextStyle(
                  color: Colors.red[700],
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {
                setState(() {
                  if (nilai == 0) {
                    nilai = 3;
                    commentRate = _commentRating.text;
                  } else {
                    nilai = nilai;
                    commentRate = _commentRating.text;
                  }
                });
                print(nilai);
                rateOrder(nilai, commentRate);
              },
            ),
          ],
        ));
  }

  void rateOrder(int nilaiSubmit, String commentRateSubmit) {
    networkOjol
        .rateOrder(
      widget.dataHistoryOrder?.idOrder.toString() ?? "",
      nilaiSubmit.toString(),
      commentRateSubmit,
    )
        .then((response) {
      if (response.result == "true") {
        Toast.show(response.message, context);
        Navigator.of(context).pushNamedAndRemoveUntil(
            UtamaScreen.tag, (Route<dynamic> route) => false);
      } else {
        Toast.show(response.message, context);
      }
    });
  }

  // void completeBooking(BuildContext context) {
  //   networkOjol
  //       .completeBooking(
  //           iduser, widget.dataHistoryOrder.idOrder.toString(), token, device)
  //       .then((response) {
  //     if (response.result == "true") {
  //       Toast.show(response.msg, context);
  //       Navigator.of(context).pushNamed(HistoryScreen.tag);
  //     } else {
  //       Toast.show(response.msg, context);
  //     }
  //   });
  // }
}

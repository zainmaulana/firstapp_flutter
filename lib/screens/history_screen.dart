import 'package:vmscustomer/helpers/general_helper.dart';
import 'package:vmscustomer/models/model_orderhistory.dart';
import 'package:vmscustomer/network/network_ojol.dart';
import 'package:vmscustomer/widgets/my_history.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryScreen extends StatefulWidget {
  static String tag = "history-page";

  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen>
    with SingleTickerProviderStateMixin {
  NetworkOjol networkOjol = NetworkOjol();
  int iduser;
  String token, device;
  List<DataHistoryOrder> dataHistoryOrder;
  TabController controller;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = TabController(length: 2, vsync: this);
    controller.addListener(() {
      setState(() {
        switch (controller.index) {
          case 0:
            return getHistory("4");
            break;
          case 1:
            return getHistory("5");
            break;
        }
      });
    });
    getPref();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  Future<void> getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    iduser = pref.getInt("iduser");
    token = pref.getString("token");
    device = await getID();
    print("Token : " + token + "\n Device : " + device);
    getHistory("4");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Order History"),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(48),
          child: Material(
            color: Colors.white,
            child: TabBar(
              controller: controller,
              tabs: <Widget>[
                Tab(
                  text: "COMPLETE",
                ),
                Tab(
                  text: "CANCEL",
                ),
              ],
              indicatorColor: Colors.red[700],
              labelColor: Colors.red[700],
            ),
          ),
        ),
      ),
      body: TabBarView(controller: controller, children: <Widget>[
        RefreshIndicator(
            child: Container(
              margin: EdgeInsets.all(10),
              child: History(
                dataHistoryOrder: dataHistoryOrder,
              ),
            ),
            onRefresh: () => getHistory("4")),
        RefreshIndicator(
            child: Container(
              margin: EdgeInsets.all(10),
              child: History(
                dataHistoryOrder: dataHistoryOrder,
              ),
            ),
            onRefresh: () => getHistory("5")),
      ]),
    );
  }

  getHistory(String status) {
    networkOjol.getHistoryOrder(iduser.toString(), status).then((response) {
      if (response.result == "true") {
        print("hasil " + response.data[0].idOrderStatus.toString());
        setState(() {
          dataHistoryOrder = response.data;
        });
      } else {
        print("tidak ada hasil");
        setState(() {
          dataHistoryOrder = null;
        });
      }
    });
  }
}

class ModelCheckOrder {
  String result;
  String message;
  Data data;

  ModelCheckOrder({this.result, this.message, this.data});

  ModelCheckOrder.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int idOrder;
  int idDriver;

  Data({
    this.idOrder,
    this.idDriver,
  });

  Data.fromJson(Map<String, dynamic> json) {
    idOrder = json['id_order'];
    idDriver = json['id_driver'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_order'] = this.idOrder;
    data['id_driver'] = this.idDriver;
    return data;
  }
}

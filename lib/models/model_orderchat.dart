class ModelChatEach {
  String result;
  String message;
  List<DataChatOrder> data;

  ModelChatEach({this.result, this.message, this.data});

  ModelChatEach.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataChatOrder>();
      json['data'].forEach((v) {
        data.add(new DataChatOrder.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataChatOrder {
  int idOrder;
  String chat;
  String tglChat;
  int idUser;
  String noTelp;
  String orderCode;
  String orderDate;
  int idOrderStatus;
  String namaDriver;
  String imageDriver;
  String namaUnit;
  String nopolUnit;
  String namaKaryawan;
  String lastChat;
  String noTelpVs;

  DataChatOrder(
      {this.idOrder,
      this.chat,
      this.tglChat,
      this.idUser,
      this.orderCode,
      this.orderDate,
      this.idOrderStatus,
      this.namaDriver,
      this.imageDriver,
      this.namaUnit,
      this.nopolUnit,
      this.namaKaryawan,
      this.noTelp,
      this.lastChat,
      this.noTelpVs});

  DataChatOrder.fromJson(Map<String, dynamic> json) {
    idOrder = json['id_order'];
    chat = json['chat'];
    tglChat = json['tgl_chat'];
    idUser = json['id_user'];
    orderCode = json['order_code'];
    orderDate = json['order_date'];
    idOrderStatus = json['id_order_status'];
    namaDriver = json['nama_driver'];
    imageDriver = json['image_driver'];
    namaUnit = json['nama_unit'];
    nopolUnit = json['nopol_unit'];
    namaKaryawan = json['nama_karyawan'];
    noTelp = json['no_telp'];
    lastChat = json['last_chat'];
    noTelpVs = json['no_telp_vs'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_order'] = this.idOrder;
    data['chat'] = this.chat;
    data['tgl_chat'] = this.tglChat;
    data['id_user'] = this.idUser;
    data['order_code'] = this.orderCode;
    data['order_date'] = this.orderDate;
    data['id_order_status'] = this.idOrderStatus;
    data['nama_driver'] = this.namaDriver;
    data['image_driver'] = this.imageDriver;
    data['nama_unit'] = this.namaUnit;
    data['nopol_unit'] = this.nopolUnit;
    data['nama_karyawan'] = this.namaKaryawan;
    data['no_telp'] = this.noTelp;
    data['last_chat'] = this.lastChat;
    data['no_telp_vs'] = this.noTelpVs;
    return data;
  }
}

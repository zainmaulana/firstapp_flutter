class ModelGetDriver {
  String result;
  String message;
  Data data;

  ModelGetDriver({this.result, this.message, this.data});

  ModelGetDriver.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int idLokasiDriver;
  int idDriver;
  int idOrder;
  String lastLatitude;
  String lastLongitude;
  String lastLocation;
  String createdOn;
  String namaDriver;
  String namaUnit;
  String nopolUnit;
  int idOrderStatus;
  String imageDriver;
  String startDate;
  String namaKaryawan;
  String orderCode;

  Data({
    this.idLokasiDriver,
    this.idDriver,
    this.idOrder,
    this.lastLatitude,
    this.lastLongitude,
    this.lastLocation,
    this.createdOn,
    this.namaDriver,
    this.namaUnit,
    this.nopolUnit,
    this.idOrderStatus,
    this.imageDriver,
    this.startDate,
    this.namaKaryawan,
    this.orderCode,
  });

  Data.fromJson(Map<String, dynamic> json) {
    idLokasiDriver = json['id_lokasi_driver'];
    idDriver = json['id_driver'];
    idOrder = json['id_order'];
    lastLatitude = json['last_latitude'];
    lastLongitude = json['last_longitude'];
    lastLocation = json['last_location'];
    createdOn = json['created_on'];
    namaDriver = json['nama_driver'];
    namaUnit = json['nama_unit'];
    nopolUnit = json['nopol_unit'];
    idOrderStatus = json['id_order_status'];
    imageDriver = json['image_driver'];
    startDate = json['start_date'];
    namaKaryawan = json['nama_karyawan'];
    orderCode = json['order_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_lokasi_driver'] = this.idLokasiDriver;
    data['id_driver'] = this.idDriver;
    data['id_order'] = this.idOrder;
    data['last_latitude'] = this.lastLatitude;
    data['last_longitude'] = this.lastLongitude;
    data['last_location'] = this.lastLocation;
    data['created_on'] = this.createdOn;
    data['nama_driver'] = this.namaDriver;
    data['nama_unit'] = this.namaUnit;
    data['nopol_unit'] = this.nopolUnit;
    data['id_order_status'] = this.idOrderStatus;
    data['image_driver'] = this.imageDriver;
    data['start_date'] = this.startDate;
    data['nama_karyawan'] = this.namaKaryawan;
    data['order_code'] = this.orderCode;
    return data;
  }
}

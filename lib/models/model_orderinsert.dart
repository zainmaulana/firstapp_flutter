class ModelInsertOrder {
  String result;
  String message;
  Data data;

  ModelInsertOrder({this.result, this.message, this.data});

  ModelInsertOrder.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String idOrderStatus;
  String idOrderType;
  int idKaryawan;
  String orderCode;
  String orderDate;
  String startDate;
  String jumlahPenumpang;
  String keperluan;
  String pickupLatitude;
  String pickupLongitude;
  String pickupLocation;
  String dropLatitude;
  String dropLongitude;
  String dropLocation;
  String realDistance;
  String realDuration;
  String catatan;
  String lastUpdatedOn;
  int idOrder;

  Data(
      {this.idOrderStatus,
      this.idOrderType,
      this.idKaryawan,
      this.orderCode,
      this.orderDate,
      this.startDate,
      this.jumlahPenumpang,
      this.keperluan,
      this.pickupLatitude,
      this.pickupLongitude,
      this.pickupLocation,
      this.dropLatitude,
      this.dropLongitude,
      this.dropLocation,
      this.realDistance,
      this.realDuration,
      this.catatan,
      this.lastUpdatedOn,
      this.idOrder});

  Data.fromJson(Map<String, dynamic> json) {
    idOrderStatus = json['id_order_status'];
    idOrderType = json['id_order_type'];
    idKaryawan = json['id_karyawan'];
    orderCode = json['order_code'];
    orderDate = json['order_date'];
    startDate = json['start_date'];
    jumlahPenumpang = json['jumlah_penumpang'];
    keperluan = json['keperluan'];
    pickupLatitude = json['pickup_latitude'];
    pickupLongitude = json['pickup_longitude'];
    pickupLocation = json['pickup_location'];
    dropLatitude = json['drop_latitude'];
    dropLongitude = json['drop_longitude'];
    dropLocation = json['drop_location'];
    realDistance = json['real_distance'];
    realDuration = json['real_duration'];
    catatan = json['catatan'];
    lastUpdatedOn = json['last_updated_on'];
    idOrder = json['id_order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_order_status'] = this.idOrderStatus;
    data['id_order_type'] = this.idOrderType;
    data['id_karyawan'] = this.idKaryawan;
    data['order_code'] = this.orderCode;
    data['order_date'] = this.orderDate;
    data['start_date'] = this.startDate;
    data['jumlah_penumpang'] = this.jumlahPenumpang;
    data['keperluan'] = this.keperluan;
    data['pickup_latitude'] = this.pickupLatitude;
    data['pickup_longitude'] = this.pickupLongitude;
    data['pickup_location'] = this.pickupLocation;
    data['drop_latitude'] = this.dropLatitude;
    data['drop_longitude'] = this.dropLongitude;
    data['drop_location'] = this.dropLocation;
    data['real_distance'] = this.realDistance;
    data['real_duration'] = this.realDuration;
    data['catatan'] = this.catatan;
    data['last_updated_on'] = this.lastUpdatedOn;
    data['id_order'] = this.idOrder;
    return data;
  }
}

class ModelCheckBooking {
  String result;
  String msg;
  String driver;

  ModelCheckBooking({this.result, this.msg, this.driver});

  ModelCheckBooking.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    msg = json['msg'];
    driver = json['driver'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['msg'] = this.msg;
    data['driver'] = this.driver;
    return data;
  }
}

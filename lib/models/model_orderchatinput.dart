class ModelOrderChatInput {
  String result;
  String message;
  Data data;

  ModelOrderChatInput({this.result, this.message, this.data});

  ModelOrderChatInput.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String idUser;
  String chat;
  String idOrder;
  String createdAt;
  int idChat;

  Data({this.idUser, this.chat, this.idOrder, this.createdAt, this.idChat});

  Data.fromJson(Map<String, dynamic> json) {
    idUser = json['id_user'];
    chat = json['chat'];
    idOrder = json['id_order'];
    createdAt = json['created_at'];
    idChat = json['id_chat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_user'] = this.idUser;
    data['chat'] = this.chat;
    data['id_order'] = this.idOrder;
    data['created_at'] = this.createdAt;
    data['id_chat'] = this.idChat;
    return data;
  }
}

class ModelGetProfile {
  String result;
  String message;
  DataProfile data;

  ModelGetProfile({this.result, this.message, this.data});

  ModelGetProfile.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    data = json['data'] != null ? new DataProfile.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataProfile {
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String password;
  String rememberToken;
  String createdAt;
  String updatedAt;
  int idRole;
  int idKaryawan;
  int idDriver;
  String fcmToken;
  String device;
  String namaKaryawan;
  String nikKaryawan;
  String namaDivisi;

  DataProfile(
      {this.id,
      this.name,
      this.email,
      this.emailVerifiedAt,
      this.password,
      this.rememberToken,
      this.createdAt,
      this.updatedAt,
      this.idRole,
      this.idKaryawan,
      this.idDriver,
      this.fcmToken,
      this.device,
      this.namaKaryawan,
      this.nikKaryawan,
      this.namaDivisi});

  DataProfile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    password = json['password'];
    rememberToken = json['remember_token'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    idRole = json['id_role'];
    idKaryawan = json['id_karyawan'];
    idDriver = json['id_driver'];
    fcmToken = json['fcm_token'];
    device = json['device'];
    namaKaryawan = json['nama_karyawan'];
    nikKaryawan = json['nik_karyawan'];
    namaDivisi = json['nama_divisi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['password'] = this.password;
    data['remember_token'] = this.rememberToken;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['id_role'] = this.idRole;
    data['id_karyawan'] = this.idKaryawan;
    data['id_driver'] = this.idDriver;
    data['fcm_token'] = this.fcmToken;
    data['device'] = this.device;
    data['nama_karyawan'] = this.namaKaryawan;
    data['nik_karyawan'] = this.nikKaryawan;
    data['nama_divisi'] = this.namaDivisi;
    return data;
  }
}

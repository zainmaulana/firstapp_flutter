class ModelAuth {
  String result;
  String message;
  String token;
  int id;
  Data data;

  ModelAuth({this.result, this.message, this.token, this.id, this.data});

  ModelAuth.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    token = json['token'];
    id = json['id'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    data['token'] = this.token;
    data['id'] = this.id;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String createdAt;
  String updatedAt;
  int idRole;
  int idKaryawan;
  int idDriver;
  String fcmToken;
  String device;

  Data(
      {this.id,
      this.name,
      this.email,
      this.emailVerifiedAt,
      this.createdAt,
      this.updatedAt,
      this.idRole,
      this.idKaryawan,
      this.idDriver,
      this.fcmToken,
      this.device});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    idRole = json['id_role'];
    idKaryawan = json['id_karyawan'];
    idDriver = json['id_driver'];
    fcmToken = json['fcm_token'];
    device = json['device'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['id_role'] = this.idRole;
    data['id_karyawan'] = this.idKaryawan;
    data['id_driver'] = this.idDriver;
    data['fcm_token'] = this.fcmToken;
    data['device'] = this.device;
    return data;
  }
}

class ModelGetKeperluan {
  String result;
  String message;
  List<DataKeperluan> data;

  ModelGetKeperluan({this.result, this.message, this.data});

  ModelGetKeperluan.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataKeperluan>();
      json['data'].forEach((v) {
        data.add(new DataKeperluan.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataKeperluan {
  int idKeperluan;
  String namaKeperluan;
  String createdAt;

  DataKeperluan({this.idKeperluan, this.namaKeperluan, this.createdAt});

  DataKeperluan.fromJson(Map<String, dynamic> json) {
    idKeperluan = json['id_keperluan'];
    namaKeperluan = json['nama_keperluan'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_keperluan'] = this.idKeperluan;
    data['nama_keperluan'] = this.namaKeperluan;
    data['created_at'] = this.createdAt;
    return data;
  }
}

class ModelNotificationOrder {
  String result;
  String message;
  List<DataNotifOrder> data;

  ModelNotificationOrder({this.result, this.message, this.data});

  ModelNotificationOrder.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataNotifOrder>();
      json['data'].forEach((v) {
        data.add(new DataNotifOrder.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataNotifOrder {
  int idNotif;
  int idUser;
  String notif;
  String createdAt;
  int idOrder;
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String password;
  String rememberToken;
  String updatedAt;
  int idRole;
  int idKaryawan;
  int idDriver;
  String fcmToken;
  String device;
  String orderCode;
  String tglNotif;

  DataNotifOrder(
      {this.idNotif,
      this.idUser,
      this.notif,
      this.createdAt,
      this.idOrder,
      this.id,
      this.name,
      this.email,
      this.emailVerifiedAt,
      this.password,
      this.rememberToken,
      this.updatedAt,
      this.idRole,
      this.idKaryawan,
      this.idDriver,
      this.fcmToken,
      this.device,
      this.orderCode,
      this.tglNotif});

  DataNotifOrder.fromJson(Map<String, dynamic> json) {
    idNotif = json['id_notif'];
    idUser = json['id_user'];
    notif = json['notif'];
    createdAt = json['created_at'];
    idOrder = json['id_order'];
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    password = json['password'];
    rememberToken = json['remember_token'];
    updatedAt = json['updated_at'];
    idRole = json['id_role'];
    idKaryawan = json['id_karyawan'];
    idDriver = json['id_driver'];
    fcmToken = json['fcm_token'];
    device = json['device'];
    orderCode = json['order_code'];
    tglNotif = json['tgl_notif'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_notif'] = this.idNotif;
    data['id_user'] = this.idUser;
    data['notif'] = this.notif;
    data['created_at'] = this.createdAt;
    data['id_order'] = this.idOrder;
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['password'] = this.password;
    data['remember_token'] = this.rememberToken;
    data['updated_at'] = this.updatedAt;
    data['id_role'] = this.idRole;
    data['id_karyawan'] = this.idKaryawan;
    data['id_driver'] = this.idDriver;
    data['fcm_token'] = this.fcmToken;
    data['device'] = this.device;
    data['order_code'] = this.orderCode;
    data['tgl_notif'] = this.tglNotif;
    return data;
  }
}

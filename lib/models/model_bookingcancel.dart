class ModelCancelBooking {
  String result;
  String msg;

  ModelCancelBooking({this.result, this.msg});

  ModelCancelBooking.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['msg'] = this.msg;
    return data;
  }
}

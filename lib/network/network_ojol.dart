import 'dart:convert';
import 'package:vmscustomer/models/model_auth.dart';
import 'package:vmscustomer/models/model_ordercancel.dart';
import 'package:vmscustomer/models/model_orderchange.dart';
import 'package:vmscustomer/models/model_orderchat.dart';
import 'package:vmscustomer/models/model_orderchatinput.dart';
import 'package:vmscustomer/models/model_ordercheck.dart';
import 'package:vmscustomer/models/model_ordergetdetail.dart';
import 'package:vmscustomer/models/model_getdriver.dart';
import 'package:vmscustomer/models/model_getprofile.dart';
import 'package:vmscustomer/models/model_ordergetkeperluan.dart';
import 'package:vmscustomer/models/model_orderhistory.dart';
import 'package:vmscustomer/models/model_orderinsert.dart';
import 'package:vmscustomer/models/model_ordernotification.dart';
import 'package:vmscustomer/models/model_orderrate.dart';
import 'package:http/http.dart' as http;

class NetworkOjol {
  static String _host = "vms.hutamakarya.com";

  // Future<ModelRegister> registerCustomer(
  //     String name, String email, String password) async {
  //   final url = Uri.http(_host, "serverojol/api/daftar");
  //   final response = await http
  //       .post(url, body: {"nama": name, "email": email, "password": password});
  //   if (response.statusCode == 200) {
  //     ModelRegister responseRegis =
  //         ModelRegister.fromJson(jsonDecode(response.body));
  //     return responseRegis;
  //   } else {
  //     return null;
  //   }
  // }

  Future<ModelAuth> loginCustomer(
    String email,
    String password,
    String device,
  ) async {
    final url = Uri.https(_host, "api/login_user");
    final response = await http.post(url, body: {
      "r_email": email,
      "r_password": password,
      "r_device": device,
    });
    if (response.statusCode == 200) {
      ModelAuth responseData = ModelAuth.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelHistoryOrder> registerFcm(
    String iduser,
    String fcm,
  ) async {
    final uri = Uri.https(_host, "api/register_fcm");
    final response = await http.post(uri, body: {
      "r_id_user": iduser,
      "r_fcm_token": fcm,
    });
    if (response.statusCode == 200) {
      ModelHistoryOrder responseData =
          ModelHistoryOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelGetProfile> getProfileUser(
    String id,
  ) async {
    final url = Uri.https(_host, "api/get_data_user");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelGetProfile responseData =
          ModelGetProfile.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelGetDetailOrder> getDetailOrderUser(
    String id,
  ) async {
    final url = Uri.https(_host, "api/detail_order");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelGetDetailOrder responseData =
          ModelGetDetailOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelGetDetailOrder> getCheckDetailOrderUser(
    String id,
  ) async {
    final url = Uri.https(_host, "api/check_do");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelGetDetailOrder responseData =
          ModelGetDetailOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelHistoryOrder> getHistoryOrder(
    String id,
    String status,
  ) async {
    final url = Uri.https(_host, "api/list_order_user");
    final response = await http.post(url, body: {
      "r_id_user": id,
      "r_id_order_status": status,
    });
    if (response.statusCode == 200) {
      ModelHistoryOrder responseData =
          ModelHistoryOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelHistoryOrder> getHistoryOrderEach(
    String id,
    String idOrder,
  ) async {
    final url = Uri.https(_host, "api/order_user");
    final response = await http.post(url, body: {
      "r_id_user": id,
      "r_id_order": idOrder,
    });
    if (response.statusCode == 200) {
      ModelHistoryOrder responseData =
          ModelHistoryOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelCancelOrder> cancelOrder(
    String idOrder,
    String commentCancel,
  ) async {
    final url = Uri.https(_host, "api/cancel_order");
    final response = await http.post(url, body: {
      "r_id_order": idOrder,
      "r_comment_cancel": commentCancel,
    });
    if (response.statusCode == 200) {
      ModelCancelOrder responseData =
          ModelCancelOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelChangeOrder> changeOrder(
    String idOrder,
    String newSchedule,
  ) async {
    final url = Uri.https(_host, "api/change_order");
    final response = await http.post(url, body: {
      "r_id_order": idOrder,
      "r_start_date": newSchedule,
    });
    if (response.statusCode == 200) {
      ModelChangeOrder responseData =
          ModelChangeOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  //API New Insert Order
  Future<ModelInsertOrder> insertOrderNew(
    String idOrderStatus,
    String idOrderType,
    String id,
    String jumlahPenumpang,
    String keperluan,
    String pickupLat,
    String pickupLng,
    String pickupLoc,
    String dropLat,
    String dropLng,
    String dropLoc,
    String realDistance,
    String realDuration,
    String catatan,
    String dateSchedule,
  ) async {
    final url = Uri.https(_host, "api/insert_order_new");
    final response = await http.post(url, body: {
      "r_id_order_status": idOrderStatus,
      "r_id_order_type": idOrderType,
      "r_id_user": id,
      "r_jumlah_penumpang": jumlahPenumpang,
      "r_keperluan": keperluan,
      "r_pickup_latitude": pickupLat,
      "r_pickup_longitude": pickupLng,
      "r_pickup_location": pickupLoc,
      "r_drop_latitude": dropLat,
      "r_drop_longitude": dropLng,
      "r_drop_location": dropLoc,
      "r_real_distance": realDistance,
      "r_real_duration": realDuration,
      "r_catatan": catatan,
      "r_date_schedule": dateSchedule,
    });

    if (response.statusCode == 200) {
      ModelInsertOrder responseData =
          ModelInsertOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelInsertOrder> insertOrder(
    String idOrderStatus,
    String idOrderType,
    String id,
    String jumlahPenumpang,
    String keperluan,
    String pickupLat,
    String pickupLng,
    String pickupLoc,
    String dropLat,
    String dropLng,
    String dropLoc,
    String realDistance,
    String realDuration,
    String catatan,
    String dateSchedule,
  ) async {
    final url = Uri.https(_host, "api/insert_order");
    final response = await http.post(url, body: {
      "r_id_order_status": idOrderStatus,
      "r_id_order_type": idOrderType,
      "r_id_user": id,
      "r_jumlah_penumpang": jumlahPenumpang,
      "r_keperluan": keperluan,
      "r_pickup_latitude": pickupLat,
      "r_pickup_longitude": pickupLng,
      "r_pickup_location": pickupLoc,
      "r_drop_latitude": dropLat,
      "r_drop_longitude": dropLng,
      "r_drop_location": dropLoc,
      "r_real_distance": realDistance,
      "r_real_duration": realDuration,
      "r_catatan": catatan,
      "r_date_schedule": dateSchedule,
    });

    if (response.statusCode == 200) {
      ModelInsertOrder responseData =
          ModelInsertOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelCheckOrder> checkOrder(
    String idOrder,
  ) async {
    final uri = Uri.https(_host, "api/check_order");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
    });
    if (response.statusCode == 200) {
      ModelCheckOrder responseData =
          ModelCheckOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelCheckOrder> checkStartOrder(
    String idOrder,
  ) async {
    final uri = Uri.https(_host, "api/check_startorder");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
    });
    if (response.statusCode == 200) {
      ModelCheckOrder responseData =
          ModelCheckOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelGetDriver> getDriver(
    String idOrder,
  ) async {
    final uri = Uri.https(_host, "api/get_driver");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
    });
    if (response.statusCode == 200) {
      ModelGetDriver responseData =
          ModelGetDriver.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelRateOrder> rateOrder(
    String idOrder,
    String idRating,
    String commentRating,
  ) async {
    final uri = Uri.https(_host, "api/rate_order");
    final response = await http.post(uri, body: {
      "r_id_order": idOrder,
      "r_id_rating": idRating,
      "r_comment_rating": commentRating,
    });
    if (response.statusCode == 200) {
      ModelRateOrder responseData =
          ModelRateOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelNotificationOrder> getNotifUser(
    String id,
  ) async {
    final url = Uri.https(_host, "api/list_notif");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelNotificationOrder responseData =
          ModelNotificationOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelOrderChatInput> inputChat(
    String idOrder,
    String id,
    String chatText,
  ) async {
    final url = Uri.https(_host, "api/chatuser");
    final response = await http.post(url, body: {
      "r_id_order": idOrder,
      "r_id_user": id,
      "r_chat": chatText,
    });
    if (response.statusCode == 200) {
      ModelOrderChatInput responseData =
          ModelOrderChatInput.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelChatEach> getEachChat(
    String id,
  ) async {
    final url = Uri.https(_host, "api/each_chat");
    final response = await http.post(url, body: {
      "r_id_user": id,
    });
    if (response.statusCode == 200) {
      ModelChatEach responseData =
          ModelChatEach.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelChatEach> getChats(
    String idOrder,
  ) async {
    final url = Uri.https(_host, "api/chat_room");
    final response = await http.post(url, body: {
      "r_id_order": idOrder,
    });
    if (response.statusCode == 200) {
      ModelChatEach responseData =
          ModelChatEach.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelGetKeperluan> getKeperluan() async {
    final url = Uri.https(_host, "api/masterdata/keperluan");
    final response = await http.get(url);
    if (response.statusCode == 200) {
      ModelGetKeperluan responseData =
          ModelGetKeperluan.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  //API Ganjil Genap
  Future<ModelInsertOrder> insertOrderGage(
    String idOrderStatus,
    String idOrderType,
    String id,
    String jumlahPenumpang,
    String keperluan,
    String pickupLat,
    String pickupLng,
    String pickupLoc,
    String dropLat,
    String dropLng,
    String dropLoc,
    String realDistance,
    String realDuration,
    String catatan,
    String dateSchedule,
  ) async {
    final url = Uri.https(_host, "api/insert_order_gg");
    final response = await http.post(url, body: {
      "r_id_order_status": idOrderStatus,
      "r_id_order_type": idOrderType,
      "r_id_user": id,
      "r_jumlah_penumpang": jumlahPenumpang,
      "r_keperluan": keperluan,
      "r_pickup_latitude": pickupLat,
      "r_pickup_longitude": pickupLng,
      "r_pickup_location": pickupLoc,
      "r_drop_latitude": dropLat,
      "r_drop_longitude": dropLng,
      "r_drop_location": dropLoc,
      "r_real_distance": realDistance,
      "r_real_duration": realDuration,
      "r_catatan": catatan,
      "r_date_schedule": dateSchedule,
    });

    if (response.statusCode == 200) {
      ModelInsertOrder responseData =
          ModelInsertOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }

  Future<ModelCancelOrder> cancelOrderGage(
    String idOrder,
    String commentCancel,
  ) async {
    final url = Uri.https(_host, "api/cancel_gage");
    final response = await http.post(url, body: {
      "r_id_order": idOrder,
      "r_comment_cancel": commentCancel,
    });
    if (response.statusCode == 200) {
      ModelCancelOrder responseData =
          ModelCancelOrder.fromJson(jsonDecode(response.body));
      return responseData;
    } else {
      return null;
    }
  }
}
